#include "memory_management.h"

//making a null pointer
block *ptr = NULL;

void * _malloc(size_t size){
	if (size == 0)
    return NULL;

	//if ptr is = NULL
	if(ptr==NULL){
		//asking os to give more memory with sbrk
		block *newblock = sbrk(roundup(size));

		newblock->next= NULL;
		newblock->prev = NULL;
		newblock->size=size;
		newblock->free = false;
		ptr = newblock;

		//returning one block header further
		return newblock+1;
	}
	//Will need this previous block later when allocating additional memory
	block *prevblock = ptr;

	//while finding a required block or the end of the linked list
	while(ptr!=NULL){
		if(ptr->free == true && ptr->size >= size){
			ptr->size = size;
			ptr->free = false;

			//block header is added to the correct ptr
			block *temp = ptr + 1;
			return temp;
		}
		//continue going throught the loop
		ptr = ptr->next;
	}
	//if ptr !=NULL and no free block with required size is found
	block *newblock = sbrk(roundup(size));
	newblock->next = NULL;
	newblock->prev = prevblock;
	prevblock->next = newblock;

	newblock->size = size;
	newblock-> free = false;
	//set pointer back to a previous block
	ptr = prevblock;

	//returning one block header further in heap
	return newblock + 1;
}

void _free(void * ptr){

	if(ptr==NULL)
		return;

	block *temp = ptr;
	//going back to the beginning of the header
	temp = temp - 1;
	temp->free = true;
	block *nextblock = temp->next;
	block *prevblock = temp->prev;

	//merging the blocks if the next free is true
	merge(nextblock,prevblock, temp);

	temp = sbrk(sizeof(ptr)*(-1));
}

void merge(block *nextblock, block *prevblock, block *temp)
{
	//merging the blocks if the previous free is true
	if(nextblock != NULL && nextblock->free ==true){
		temp->size += nextblock->size + sizeof(block);
		temp->next = nextblock->next;
	}
	//making sbrk argument negatibve to decrease the heap / make free
	if(prevblock != NULL && prevblock->free==true){
		prevblock->size += temp->size + sizeof(block);
		prevblock->next = temp->next;
	}
}

//for rounding up the pages
int roundup(int size)
{
	if(size==page)
		return size; //size unchanged

	else
		size = size + page - (size % page);
	return size; //changed size
}

