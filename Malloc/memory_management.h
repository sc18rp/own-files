#include <sys/types.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>

#define page 4096

typedef struct block{
	int size;
	bool free;
  //for next and previous in a double linked list
	struct block* next;
	struct block* prev;
}block;

void * _malloc(size_t size);

void _free(void * ptr);

void merge(block *nextblock, block *prevblock, block *temp);

int roundup(int size);

