#these are own created files, so I am importing everyting
from forms import *
from config import *
from models import *

from flask_mail import Message
from sqlalchemy.sql import exists
from flask import g, session
import json
import operator
#for finding most popular users
import collections
from collections import Counter

import logging

#setting the logs
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

#setting the format in which the logs will print the log info
formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(name)s:%(message)s')

#file where the log info will be stored
file_handler = logging.FileHandler('logs/appactivity.log')
file_handler.setFormatter(formatter)

# For displaying the log info to the console
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)

logger.addHandler(stream_handler)
logger.addHandler(file_handler)

#double root for both /home and / the same
@app.route("/")
@app.route("/home")
def home():
    page = request.args.get('page', 1, type=int)
    #make only 4 posts per page to be shown (user may switch between the pages by clicking buttons)
    posts=Post.query.order_by(Post.date_posted.desc()).paginate(per_page=4, page=page)
    return render_template('home.html', posts=posts)

#this route redirects to about.html - the info about the app itself
@app.route("/about")
def about():
    return render_template('about.html', title='About')

#sign up route, taking password, confirm password, email, and username
@app.route("/signup", methods=['GET', 'POST'])
def signup():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    #openning RegistrationForm in 'form' variable
    form = RegistrationForm()
    if form.validate_on_submit():
        #hashing the password , so it can be saved into the database in random chars
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        #creating the log when the user is signed
        logger.info('New account: {}'.format(user))
        #supporting user with a sensible flash message
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('signup.html', title='signup', form=form)

#after the user is registered, (s)he can log in.
@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    #if user press the submit button
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        #if user is correct and hashed password maches then the user is loged in the webapp
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            logger.info('Successful user log in: {}'.format(user))
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            #else, user is notified with sensible message and the warning log is saved
            logger.warning('Unsuccessful user log in: {}'.format(user))
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)

#simpy log out the user. Logically, it requires user to be loged in
@app.route("/logout")
@login_required
def logout():
    logger.info('User successfully logged out')
    logout_user()
    flash('Logout Successful', 'success')
    return redirect(url_for('home'))

#accounte route hold information about the user
#it can be accessed only when the specific user is logged in
@app.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    #whe checking your account, user is also able to change the username or email adress
    form = UpdateAccountForm()
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        logger.info('User data from: {}, {} changed to: {}, {}'.format(form.username.data, form.email.data, current_user.username, current_user.email))
        flash('Your account has been updated!', 'success')
        return redirect(url_for('account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    return render_template('account.html', title='Account', form=form)

#new post route, allows user to create a new post. Log in is required
@app.route("/post/new", methods=['GET', 'POST'])
@login_required
def new_post():
    form = PostForm()
    if form.validate_on_submit():
        #adding posts to a SQLALCHEMY_DATABASE_URI
        #the length of the content must be less than 500.
        if len(form.content.data)>800:
            logger.warning('Fails to create a post')
            flash("The length of a task should be less than 800 charcters", "warning")
            return redirect(url_for('new_post'))
        else:
            post = Post(title=form.title.data, content=form.content.data, author=current_user)
            db.session.add(post)
            db.session.commit()
            logger.info('Post has been created')
            flash('Your post has been created successfully!', 'success')
            return redirect(url_for('home'))
    return render_template('create_post.html', title='New post', form=form,  legend='New Post')


@app.route("/post/<int:post_id>")
def post(post_id):
    #gives post with id or gives error 404 - post doesnt exist
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.title, post=post)

#For logged in user, to update the post
@app.route("/post/<int:post_id>/update", methods=['GET', 'POST'])
@login_required
def update_post(post_id):
    #gives post with id or gives error 404 - post doesnt exist
    post = Post.query.get_or_404(post_id)
    if post.author !=current_user:
        logger.critical('User: {} illegaly tried to modify the post of {}'.format(current_user, post.author))
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        logger.info('Post with title {} has been updated to title {}'.format(post.title, form.title.data))
        logger.info('Post with content {} has been updated to content {}'.format(post.content, form.content.data))
        #changing the values in a datbase, Post Model
        post.title = form.title.data
        post.content = form.content.data
        post.updated = True
        db.session.commit()
        flash('The post has been updated!', 'success')
        return redirect(url_for('post', post_id=post.id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.content.data = post.content
    return render_template('create_post.html', title='Update post', form=form, legend='Update Post')

#Fol logged in users to be able to delete the post
@app.route("/post/<int:post_id>/delete", methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        #modifying the log with a critical message
        logger.critical('User: {} illegaly tried to delete the post of {}'.format(current_user, post.author))
        abort(403)
    db.session.delete(post)
    db.session.commit()
    logger.info('{} successfully deleted the post'.format(post.author))
    flash('Your post has been deleted!', 'success')
    #redirecting the home page
    return redirect(url_for('home'))

#only one user's posts
@app.route("/user/<string:username>")
@login_required
def user_posts(username):
    page = request.args.get('page', 1, type=int)
    #filtering the user query
    user = User.query.filter_by(username=username).first_or_404()
    #filtering the post query
    posts = Post.query.filter_by(author=user).order_by(Post.date_posted.desc()).paginate(per_page=4, page=page)
    return render_template('user_posts.html', posts=posts, user=user)

#most poopular users by the followers
@app.route("/most_popular_users")
def most_popular_users():

    #for number of followers
    most_pop_int=[]
    #for username
    most_pop_sting=[]

    users = User.query.all()
    for user in users:
        most_pop_sting.append(user.username)
        most_pop_int.append(user.followers.count())

    #zipping the files (making like a map of both)
    zipped = zip(most_pop_sting, most_pop_int)

    #listing the 'map'
    zipped = list(zipped)

    #sorting, so that the mapped list contains sorted values, so I can find most popular users
    res = sorted(zipped, key = lambda x: x[1])
    res = list(reversed(res))

    # unzipping values
    most_pop_sting, most_pop_int = zip(*res)

    return render_template('most_popular_users.html', users=most_pop_sting, followers=most_pop_int)


#filtering the post query, such that it would show only in the past 24h created posts
@app.route("/latest_posts")
def latest_posts():
    page = request.args.get('page', 1, type=int)

    #posts will be shown in between from the current time, subtracting 24
    posts=Post.query.filter(Post.date_posted >= (datetime.today() - timedelta(hours=24, minutes=0))).order_by(
    Post.date_posted.desc()).paginate(per_page=4, page=page)
    #if there is no posts, make user know
    if posts.total <= 0:
        return render_template('error_no_posts.html')
    return render_template('latest_posts.html', posts=posts)

#for resetting the password
def send_reset_email(user):
    #making a new token
    token = user.get_reset_token()
    #creating a new message to the recipients
    msg = Message('Password reset request', recipients=[user.email])
    link = url_for('reset_token', token=token, _external=True)

    msg.body = 'Please confirm your email. The link is {}'.format(link)
    #sending the message
    mail.send(msg)
    logger.info("Reset email sent to {}".format(user.email))

#for resetting the password
@app.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = ResetRequestForm()
    if form.validate_on_submit():
        #finding the first user in the query
        user = User.query.filter_by(email=form.email.data).first()
        #calling the fucntion above to sent the email
        send_reset_email(user)
        flash('Email has been sent! You must confirm', 'info')
        return redirect(url_for('login'))

    return render_template('reset_request.html', title='Reset Password', form=form)

#for changing the password
@app.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    user = User.verify_reset_token(token)
    #if the user, which has to confirm the token is not found
    if user is None:
        flash('That is an invalid or expired token', 'danger')
        return redirect(url_for('reset_request'))
    #if the user is valid, create a new ResetPasswordForm.
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        logger.info("{} updated the password".format(user))
        flash('Your password has been updated! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('reset_token.html', title='Reset Password', form=form)

#for following the user
@app.route('/follow/<username>', methods=['GET', 'POST'])
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    #if user does not exit, write in the log and show flash message
    if user is None:
        logger.error("User does not exist anymore".format(user))
        flash('User {} not found.'.format(username), 'warning')
        return redirect(url_for('home'))
    #the function does not allow to follow yourself
    if user == current_user:
        logger.warning("{} tried to follow him/herself".format(current_user))
        flash('You can\'t follow yourself!', 'warning')
        return redirect(url_for('user_posts', username=username))
    usern = current_user.follow(user)
    if usern is None:
        logger.error("{} tried to follow invalid user {}".format(current_user, user))
        flash('Cannot follow ' + username + '.', 'warning')
        return redirect(url_for('user_posts', username=username))
    db.session.add(usern)
    db.session.commit()
    logger.info("{} successfully followed {}".format(current_user, user))
    flash('You are now following ' + username + '!', 'info')
    return redirect(url_for('user_posts', username=username))

#unfollowing the user
@app.route('/unfollow/<username>', methods=['GET', 'POST'])
@login_required
def unfollow(username):
    #filtering the qery to show the first username
    user = User.query.filter_by(username=username).first()
    #if user does not exit, write in the log and show flash message
    if user is None:
        logger.error("User does not exist anymore".format(user))
        flash('User {} not found.'.format(username), 'warning')
        return redirect(url_for('home'))
    if user == current_user:
        logger.warning("{} tried to unfollow him/herself".format(current_user))
        flash('You can\'t unfollow yourself!', 'warning')
        return redirect(url_for('user_posts', username=username))
    usern = current_user.unfollow(user)
    if usern is None:
        logger.error("{} tried to unfollow invalid user {}".format(current_user, user))
        flash('Cannot unfollow ' + username + '.', 'warning')
        return redirect(url_for('user_posts', username=username))
    #modifying the database
    db.session.add(usern)
    db.session.commit()
    logger.info("{} successfully unfollowed {}".format(current_user, user))
    flash('You have stopped following ' + username + '.', 'info')
    return redirect(url_for('user_posts', username=username))

#setting the cookie
@app.route('/setcookie')
def setcookie():
    #making responce object
    resp = make_response('Setting cookie!')
    #in resp object setting a cookie
    resp.set_cookie('framework', 'flask')
    return resp

#getting the cookie
@app.route('/getcookie')
def getcookie():
    framework = request.cookies.get('framework')
    return '<h1>framework is: </h1>' + framework

#to show the current location of the user
@app.route('/location')
@login_required
def location():
    return render_template('location.html')

#error handlers, to show more user friendly message
@app.errorhandler(404)
def error_404(error):
    return render_template('404.html')

@app.errorhandler(403)
def error_403(error):
    return render_template('403.html')

@app.errorhandler(500)
def error_500(error):
    return render_template('500.html')
