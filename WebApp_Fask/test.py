#application contains all the imports and imports of imports
from app import *
import os
import unittest

class TestCase(unittest.TestCase):
    '''
    should test:
    If email/username is not taken
    If password is too showrt
    if username is too showrt
    if email is in right form
    if post content is not too long

    '''
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_user_database(self):
        self.assertEqual(User.query.all(), [])
        # hashed_password = bcrypt.generate_password_hash('somerandompassword').decode('utf-8')
        user = User(username='Rokas', email='Rokas@gmail.com', password="somepassword")
        db.session.add(user)
        db.session.commit()
        # checking if database is not empty (it should not be empty)
        assert User.query.filter_by(username='Rokas') != None
        assert User.query.filter_by(email='Rokas@gmail.com') != None
        assert User.query.filter_by(username='somepassword') != None

    def test_post_database(self):
        self.assertEqual(Post.query.all(), [])
        post = Post(title='Title', content="very very long long content content content content content content", user_id=1)
        db.session.add(post)
        db.session.commit()
        # checking if database is not empty (it should not be empty)
        assert Post.query.filter_by(user_id=1) != None

    def test_hash_password(self):
        hashed_password = bcrypt.generate_password_hash('password').decode('utf-8')
        value = bcrypt.check_password_hash(hashed_password, "not this password")
        assert value==False
        #space at the end
        value = bcrypt.check_password_hash(hashed_password, "password ")
        assert value==False
        value = bcrypt.check_password_hash(hashed_password, "password")
        assert value==True

    def test_follow(self):
        user1 = User(username='Rokas', email='RokasPranevicius@example.com', password='anything')
        user2 = User(username='Pranevicius', email='RokasP@gmail.com', password='anything')
        db.session.add(user1)
        db.session.add(user2)
        db.session.commit()
        self.assertEqual(user1.followed.all(), [])
        self.assertEqual(user1.followers.all(), [])

        user1.follow(user2)
        db.session.commit()
        self.assertTrue(user1.is_following(user2))
        self.assertEqual(1, user1.followed.count())
        self.assertEqual(user1.followed.first().username, 'Pranevicius')
        self.assertEqual(1, user2.followers.count())
        self.assertEqual(user2.followers.first().username, 'Rokas')

        user1.unfollow(user2)
        db.session.commit()
        self.assertFalse(user1.is_following(user2))
        self.assertEqual(0, user1.followed.count())
        self.assertEqual(0, user2.followers.count())

    def test_make_unique_username(self):
        # create a user and write it to the database
        user = User(username='Rokas', email='unique@example.com', password='pass')
        db.session.add(user)
        db.session.commit()
        username = User.make_unique_username('Rokas')
        assert username != 'Rokas'
        # make another user with the new username
        user = User(username=username, email='sameusername@example.com', password='pass')
        db.session.add(user)
        db.session.commit()
        username2 = User.make_unique_username('Rokas')
        assert username2 != 'Rokas'
        assert username2 != username

if __name__ == '__main__':
    unittest.main()
