#importing necessary packages for the app

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from datetime import datetime, timedelta
from flask import Flask, render_template, url_for, flash, redirect, request, abort
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager, UserMixin, login_user, current_user, logout_user, login_required

#for reseting password via email
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask_mail import Mail
from flask_admin import Admin, BaseView, expose
from flask_admin.contrib.sqla import ModelView
from flask_migrate import Migrate

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, joinedload
from datetime import datetime, timedelta

MAX_SEARCH_RESULTS = 50
app = Flask(__name__)

#randomly generated secret key
app.config['SECRET_KEY'] = '2791628bb0b13ce0c676dfde280ba245'

#configuring the database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
db = SQLAlchemy(app)

#configuring the migrate
migrate = Migrate(app, db)

#configuring the password hasing algorithms to the app
bcrypt = Bcrypt(app)

#defining the login manager
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'

# set optional bootswatch theme
app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
admin = Admin(app, name='Admin', template_mode='bootstrap3')

#sending the email properties
app.config['TESTING'] = False
app.config['MAIL_SERVER'] = 'smtp.gmail.com' #gmail
app.config['MAIL_PORT'] = 465
app.config['MAIL_DEBUG'] = True
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

#creating any email (temporary)
app.config['MAIL_USERNAME'] = 'ViktarasUsbonis@gmail.com'
app.config['MAIL_PASSWORD'] = 'veryanonymoussecretsender'
app.config['MAIL_DEFAULT_SENDER'] = 'ViktarasUsbonis@gmail.com'
app.config['MAIL_MAX_EMAILS'] = None

app.config['MAIL_ASCII_ATTACHEMENTS'] = False #convertes the file name/convert ->ascii

mail = Mail(app)
mail.init_app(app)
