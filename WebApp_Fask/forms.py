#importing the necessary data from the config file mostly
from models import *
from config import *

import logging

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

#making the formatter show message, levelname and custom created mesasge
formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(message)s')
file_handler = logging.FileHandler('logs/forms.log')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

#for console to show logs
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)

logger.addHandler(stream_handler)

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

#creating the registration form with various validators and sensible fields
class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')

    #validators, checking if the username and email are valid
    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            logger.error('User tried to sign up with taken username: {}'.format(user.username))
            raise ValidationError('The username is already taken!')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            logger.error('User tried to sign up with taken email: {}'.format(user.email))
            raise ValidationError('The email is already taken!')

#log in form used for user log in, also containing sensible validators and fields
class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')

#when the user want to update the account, this form should be called in app.py
class UpdateAccountForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Update')

    #checking if newly created username and email are valid
    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(username=username.data).first()
            if user:
                logger.warning('User tried to update username with already taken: {}'.format(self.username))
                raise ValidationError('That username is taken. Please choose a different one.')

    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email=email.data).first()
            if user:
                logger.warning('User tried to update email with already taken: {}'.format(self.email))
                raise ValidationError('That email is taken. Please choose a different one.')

class PostForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    content = TextAreaField('Content', validators=[DataRequired()])
    submit = SubmitField('Post')

class ResetRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            logger.critical('User requested reset email with unknown email: {}'.format(user))
            raise ValidationError('There is no account with that email. You must register first.')

#form, used when the user wants to reset the password.
class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')

#adding User and Post database models to the admin
#in the admin mode, I can controll the users more easily
admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Post, db.session))
