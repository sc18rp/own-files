#include "define_variables.h"
#include "algorithms.h"

int english_checker(char *input, int level_of_english)
{
  FILE *open = fopen(en_words, "r");

  char *temp_input = (char*)malloc(size*sizeof(char)); //not to change the original input
  strcpy(temp_input, input);

  //Removing characters
  remove_char(temp_input, '?');
  remove_char(temp_input, '!');
  remove_char(temp_input, '.');
  remove_char(temp_input, ',');
  remove_char(temp_input, ':');
  remove_char(temp_input, '(');
  remove_char(temp_input, ')');

  char separated[size/2][size];
  char *pt = (char*)malloc(size*sizeof(char));
  int count_word = 0, x;
  char line[25];

  //breaks string into series of tokens
  pt = strtok (temp_input, " ");

  int i=0;
  while (pt != NULL)
  {
      strcpy(separated[i], pt);
      pt = strtok (NULL, " ");
      i++;
  }
  //Now it has to be separated by space
  while (fgets(line, sizeof(line), open))
    {
      line[strlen(line)-1]='\0'; //delete new line
      for(x=0; x<i; x++)
      {
        lower_string(separated[x]);//all letters to be lower case just like in text file

        if(strcmp(line, separated[x])==0 || similarity(line, separated[x], 0)>=level_of_english)
        {
          count_word++;
          break;
        }
      }
    }
  free(pt);
  free(temp_input);
  if(count_word == 0)
  {
    fclose(open);
    return 2; //Super poor english
  }
  if(count_word >= i)
  {
    fclose(open);
    return 1; //The sentence is written in english
  }
  fclose(open);
  return 0; //The sentence is a nonsence
}

bool name_checker(char *input)
{
  if(strlen(input)>size)
    return NULL;

  FILE *open = fopen(names, "r");

  char temp_input[size]; //not to change original input
  strcpy(temp_input, input);

  remove_char(temp_input, '?');
  remove_char(temp_input, '!');
  remove_char(temp_input, '.');
  remove_char(temp_input, ',');
  remove_char(temp_input, ':');
  remove_char(temp_input, '(');
  remove_char(temp_input, ')');

  char *me = "ME";
  char *my = "MY";
  char *q = "?";
  char *why = "WHY", *how = "HOW";
  char *is ="IS", *are = "ARE";

  char separated[size/2][size];
  char *pt;
  int x;

  char name[25]; //the word in a database

  //breaks string into series of tokens
  pt = strtok (temp_input, " ");

  int i=0;
  while (pt != NULL)
  {
      strcpy(separated[i], pt);
      pt = strtok (NULL, " ");
      i++;
  }

  bool is_name = false;
  //Now it is separated by space
  while (fgets(name, sizeof(name), open))
    {
      name[strlen(name)-1]='\0'; //delete new line
      for(x=0; x<i; x++)
      {
        //all letters to be lower case just like in text file
        higher_string(separated[x]);
        if(strcmp(name, separated[x])==0)
        {
          is_name =  true;
          break;
        }
      }
      if(is_name)break;
    }
    int t=0;

    //Only if name appeared
    if(is_name)
    {
      //First check for Negative words
      for(x=0; x<i; x++)
      {
        if(negative_word_appears(separated[x]))
        {
          if(strstr(separated[0], is)!=NULL)
          {
            D_name_terrible_is(name);
            return true;
          }
          for(t; t<i; t++)
          if(strstr(separated[t], why) || strstr(separated[t], how))
          {
            D_name_terriblequestion(name);
            return true;
          }
          D_name_terrible(name, separated[x]);
          return true;
        }
      }
      //Then check for question
      if(strstr(separated[0], is) || strstr(separated[0], are))
      {
        D_name_isare(name);
        return true;
      }
      else if(strstr(separated[0], why) || strstr(separated[0], how))
      {
        D_name_whyhow(name);
        return true;
      }
      else
      {
        //Will pint simple sentence with that name
        D_name(name);
        return true;
      }
    }
  //No name appeared
  return false;
}
