#include "define_variables.h"
#include "all_chars.h"
#include "algorithms.h"

//srand(time(NULL)); for making the time change always
void D_name_isare(char *input)
{
  srand(time(NULL));
  lower_string(input);
  input[0] = toupper(input[0]);

  //Making random number, to pick the answer randomly
  int random = rand() % 5;
  switch (random) {
    case 0:
    printf("Dummy: There is no %s here, lets talk together.\n", input);break;
    case 1:
    printf("Dummy: How can I know, ask %s.\n", input);break;
    case 2:
    printf("Dummy: You should Probably ask %s.\n", input);break;
    case 3:
    printf("Dummy: Better ask %s's friends.\n", input);break;
    case 4:
    printf("Dummy: You better talk about yourself\n");break;
  }
}

void D_name(char *input)
{
  srand(time(NULL));
  lower_string(input);
  input[0] = toupper(input[0]);

  int random = rand() % 5;
  switch (random) {
    case 0:
    printf("Dummy: Is %s a good person?\n", input);break;
    case 1:
    printf("Dummy: Who is %s?\n", input);break;
    case 2:
    printf("Dummy: I think I know who %s is.\n", input);break;
    case 3:
    printf("Dummy: Is %s a rock star?\n", input);break;
    case 4:
    printf("Dummy: Oh yea, I remember %s!\n", input);break;
  }
}

void D_chatbot_lost(void)
{
  printf("Dummy: %s\n", lost[rand()%(sizeof(lost)/sizeof(lost[0]))]);
}

void D_poor_en(void)
{
  printf("Dummy: %s\n", poor_english[rand()%(sizeof(poor_english)/sizeof(poor_english[0]))]);
}

void D_super_poor_en(void)
{
  srand(time(NULL));
  int i=10, random = rand() % 2;
  char random_letter;
  char random_string[11];

  switch (random) {
    case 0:
    while(i>=0)
    {
      random_letter = 'A' + (rand() % 58);
      random_string[i] = random_letter;
      i--;
    }
    printf("Dummy: %s how do you like that haha\n", random_string);break;
    case 1:
    printf("Dummy: %s\n", super_poor_english[rand()%(sizeof(super_poor_english)/sizeof(super_poor_english[0]))]);
    break;
  }
}

void D_start_name(void)
{
  printf("Dummy: %s\n", answer_meeting[rand()%(sizeof(answer_meeting)/sizeof(answer_meeting[0]))]);
}

void D_start_wow(void)
{
  printf("Dummy: %s\n", answer_wow[rand()%(sizeof(answer_wow)/sizeof(answer_wow[0]))]);
}

void D_start_are(void)
{
  printf("Dummy: %s\n", answer_are[rand()%(sizeof(answer_are)/sizeof(answer_are[0]))]);
}

void D_start_is(void)
{
  printf("Dummy: %s\n", answer_is[rand()%(sizeof(answer_is)/sizeof(answer_is[0]))]);
}

void D_start_can(void)
{
  printf("Dummy: %s\n", answer_can[rand()%(sizeof(answer_can)/sizeof(answer_can[0]))]);
}

void D_start_stop(void)
{
  printf("Dummy: %s\n", answer_stop[rand()%(sizeof(answer_stop)/sizeof(answer_stop[0]))]);
}

void D_sure(void)
{
  printf("Dummy: %s\n", sure_sentences[rand()%(sizeof(sure_sentences)/sizeof(sure_sentences[0]))]);
}

void D_math(void)
{
  printf("Dummy: %s\n", math_sentences[rand()%(sizeof(math_sentences)/sizeof(math_sentences[0]))]);
}

void D_bye(void)
{
  printf("Dummy: %s\n", exit_words[rand()%(sizeof(exit_words)/sizeof(exit_words[0]))]);
  exit(0);
  //If user enters a bye word, the program just exits.
}

//This one is the very basic one.
void D_answer(char *input)
{
  printf("Dummy: %s\n", input);
}

void D_name_terrible_is(char *name)
{
  srand(time(NULL));
  lower_string(name);
  name[0] = toupper(name[0]);

  int random = rand() % 3;
  switch (random) {
    case 0:
    printf("Dummy: Oh dear, is %s really?\n", name);break;
    case 1:
    printf("Dummy: I can't believe %s is\n", name);break;
    case 2:
    printf("Dummy: What?? That's terrible news for %s's family.\n", name);break;
  }
}

void D_name_terriblequestion(char *name)
{
  srand(time(NULL));
  lower_string(name);
  name[0] = toupper(name[0]);

  int random = rand() % 2;
  switch (random) {
    case 0:
    printf("Dummy: Idk, that would be terrible news, I hope %s is fine\n", name);break;
    case 1:
    printf("Dummy: No, I haven't heard about it. I hope %s is alright\n", name);break;
  }
}

bool D_name_terrible(char *name, char *input)
{
  srand(time(NULL));
  //string - will be a negative word
  //input - one word input
  lower_string(name);
  lower_string(input);
  name[0] = toupper(name[0]);

  int random = rand() % 3;

  int i;
  for(i=0; i<sizeof(negative_words)/sizeof(negative_words[0]); i++)
  {
    lower_string(negative_words[i]);

    if(strstr(input, negative_words[i])!=NULL)
    {
      switch(random)
      {
        case 0:
        printf("Dummy: I have to say sorry, but is %s really %s?\n", name, input);break;
        case 1:
        printf("Dummy: No way! I hope %s is fine\n", name);break;
        case 2:
        printf("Dummy: Who? %s? I hope its fake news\n", name);break;
      }
      return true;
    }
  }
  return false;
}

void D_name_whyhow(char *input)
{
  srand(time(NULL));
  lower_string(input);
  input[0] = toupper(input[0]);

    int random = rand() % 4;
    switch (random) {
      case 0:
      printf("Dummy: I don't know, ask %s.\n", input);break;
      case 1:
      printf("Dummy: How can I know it, mate, ask %s's friend.\n", input);break;
      case 2:
      printf("Dummy: %s is a nice man, I am sure he made a good decision.\n", input);break;
      case 3:
      printf("Dummy: I am not %s, ask himself.\n", input);break;
    }
}

void D_same(char *input)
{
  printf("Dummy: %s?\n", input);
}

bool D_random_decision(char input[])
{
  if(happy_options(input))
  {
    printf("Dummy: %s\n", happy_mood[rand()%(sizeof(happy_mood)/sizeof(happy_mood[0]))]);
    return 1;
  }

  if(swearing_options(input))
  {
    printf("Dummy: %s\n", stopping[rand()%(sizeof(stopping)/sizeof(stopping[0]))]);
    return 1;
  }

  if(meeting_options(input))
  {
    printf("Dummy: %s\n", meeting[rand()%(sizeof(meeting)/sizeof(meeting[0]))]);
    return 1;
  }
  //this one is separate, because I will use it alone in other modules.
  if(bye_options(input))
  {
    D_bye();
  }
}

bool D_short_phrase(char *input)
{
  srand(time(NULL));
  if(strstr(input, " ")==NULL)
  {
    remove_char(input, '.');
    remove_char(input, '?');
    remove_char(input, '!');

    int random = rand() % 3;
    switch (random) {
      case 0:
      printf("Dummy: %s what? Be more clear next time.\n", input);break;
      case 1:
      printf("Dummy: What you mean by %s?\n", input);break;
      case 2:
      printf("Dummy: Please write more about %s\n", input);break;
    }
    return 1;
  }
  return 0;
}

bool negative_word_appears(char *input)
{
  int i;
  for(i=0; i<sizeof(negative_words)/sizeof(negative_words[0]); i++)
  {
    //Highing, because input is uppercase
    higher_string(negative_words[i]);
    if(strstr(input, negative_words[i]))return true;
  }
  return false;
}

bool insta_starting(char *input)
{
  //const char to char array
  char temp[size];
  strcpy(temp, input);

  char are[] = "are ";
  char is[] = "is ";
  char can[] = "can ";
  char could[] = "could ";
  char sure[] = "sure";
  char stop[] = "stop";
  char wow[] = "wow";
  char omg[] = "omg";
  char plus[] = "+", minus[] = "-";
  char name[] = "my name is";

  lower_string(temp);

  if(starts_with(temp, name))
  {
    D_start_name();
    return 1;
  }

  if(starts_with(temp, wow) || starts_with(temp, omg))
  {
    D_start_wow();
    return 1;
  }

  if(starts_with(temp, are))
  {
    D_start_are();
    return 1;
  }
  if(starts_with(temp, is))
  {
    D_start_is();
    return 1;
  }
  if(starts_with(temp, can) || starts_with(temp, could))
  {
    D_start_can();
    return 1;
  }
  if(starts_with(temp, stop))
  {
    D_start_stop();
    return 1;
  }

  char *check;
  check = strstr(temp, sure);
  //If contains 'sure' anywhere in a string
    if(check!=NULL)
    {
      D_sure();
      return 1;
     }

     if(strstr(temp, plus)!=NULL || strstr(temp, minus)!=NULL)
     {
       D_math();
       return 1;
    }
    return 0;
}

//Check if string starts with a string
bool starts_with(const char *a, const char *b)
{
   if(strncmp(a, b, strlen(b)) == 0) return true;
   return false;
}

bool happy_options(char input[])
{
  if(strlen(input)>=size)
    return NULL;

  char temp[size];
  //creating an array of char out of const
  strcpy(temp, input);

  //For ignoring capital letters
  lower_string(temp);

  bool value=false;
  int i;
  char *check;
  int rows = sizeof(happy_words)/sizeof(happy_words[0]);

  for(i=0; i<rows; i++)
  {
    check = strstr(temp, happy_words[i]);
    if(check!=NULL)
    {
      value=true;
      break;
    }
  }
  return value;
}

bool swearing_options(char input[])
{
  int i=0;
  if(strlen(input)>=size)
    return NULL;

  char temp[size];
  //creating an array of char out of const
  strcpy(temp, input);

  lower_string(temp);

  bool value=false;

  char *check;
  int rows = sizeof(swear_words)/sizeof(swear_words[0]);

  for(i=0; i<rows; i++)
  {
    check = strstr(temp, swear_words[i]);
    if(check!=NULL)
    {
      value=true;
      break;
    }
  }
  return value;
}

bool meeting_options(char input[])
{
  if(strlen(input)>=size)
    return NULL;

  char temp[size];
  //creating an array of char out of const
  strcpy(temp, input);

  lower_string(temp);
  bool value=false;

  int i;
  char *check;
  int rows = sizeof(meeting)/sizeof(meeting[0]);

  temp[0] = toupper(temp[0]); //making upper case first letter

  for(i=0; i<rows; i++)
  {
    check = strstr(temp, meeting[i]); //check if it is a substring
    if(check!=NULL)
    {
      value=true;
      break;
    }
  }
  return value;
}

bool bye_options(char input[])
{
  if(strlen(input)>=size)
    return NULL;
  char temp[size];
  strcpy(temp, input);

  lower_string(temp);
  bool value=false;

  int i;
  char *check;
  int rows = sizeof(bye_words)/sizeof(bye_words[0]);

  for(i=0; i<rows; i++)
  {
    check = strstr(temp, bye_words[i]); //check if it is a substring
    if(check!=NULL)
    {
      value=true;
      break;
    }
  }
  return value;
}
