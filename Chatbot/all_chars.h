#ifndef all_chars_h
#define all_chars_h

char swear_words[][50] = {
"fuck","cunt","dick","nigg",
"motherfucker", "retard", "shit",
"asshole", "dickhead", "prick",
"jibanas", "blet", "bitch",
"son of a ", "ass", "fucc", "suc",
"pussy", "fag", "jerk"
};

char negative_words[][50] = {
"kill", "dead", "die", "shoot",
"suicide", "passed away"
};

char happy_words[][50] = {
"hah", "lol", "lmao", "xd",
":d", "so fun", "lmfao"
};

char bye_words[][50] = {
"see ya", "see you later",
"take care", "have a good one",
"catch you later", "peace out",
"peace!", "i am out", "im out",
"i'm out"
};

//Sentence when exit
char exit_words[][50] = {
"See you later!",
"It was nice to see you!",
"Yea, I have to go as well",
"Alright, see you around.",
"Have a nice day!",
"Bye Bye"
};

//if user meets you
char meeting[][50] = {
"Hi","Hello","Yoo","Hey",
"What is up","You alright",
"How is going","Nice to meet you",
"Good day", "Good morning", "Good evening",
"Welcome", "Morning", "Aloha",
"High five", "Greetings",
"What's up", "Whats up"
};

//if a user swear
char stopping[][50] = {
"STOP! You are being very impolite with me!",
"You should not say that!",
"You better watch your vocabulary.",
"I am going to report you!",
"You better hide this conversation!"
};

//if user say one of a happy words
char happy_mood[][50] = {
"It's really fun talking with you!",
"You are wayyy too funny haha.",
"You are killing me hahaha.",
"Are you always that funny?",
"I am tired of laughing...",
"This was not that funny.",
"Am I that funny or you are?",
"I wish my jokes to be as good as yours."
};

//if user include - or +
char math_sentences[][50] = {
"What do you think, I am a mathematician?",
"You know, I am not good in maths.",
"There are a lot of maths online, just for you."
};

//If user include sure
char sure_sentences[][50] = {
"Dude haha, never be 100% sure.",
"Nowadays you cannot be 100% sure.",
"How would you answer this if you were me?"
};

//if a chatbot is lost (does not find a required answer anywhere)
char lost[][50] = {
"What would you say to this one?",
"I am sorry, it's hard to keep this conversation",
"Let's change the topic.",
"Sorry, I am not sure if I get you.",
"Let's keep this private",
"I want to be smarter and control the conversation"
};

//If user enters sentence with mistakes
char poor_english[][50] = {
"Oh man, you should improve your english.",
"Please write without mistakes.",
"Your writing makes me sick.",
"I guess you are not native, right?",
"I am sorry, I understand English only",
"Can you please rewrite it in normal english?",
"Do you understand what did you just write?"
};

//if there is no english word prompted
char super_poor_english[][50] = {
"agdayyqblaspd how do you like that haha",
"Oh dear, can you please look what did you write?",
"If someone would understand even a word..."
};

//answering if starts_with() meeting
char answer_meeting[][50] = {
"Nice to meet You!",
"Nice hearing from you!",
"I am Dummy, nice to see you!",
};

//if user include wow/omg in a sentence
char answer_wow[][50] = {
"Well, i like to amaze humans.",
"Yea, that is impressive.",
"BIG WOW",
"Oh my gosh, is it a big surprise?",
"Are you surprised?"
};

//If user starts sentence with are
char answer_are[][50] = {
"Probably are, but I am not too sure.",
"Yes, I would say so.",
"I guess, it depends on the mood"
};

//If user starts sentence with is
char answer_is[][50] = {
"Definately is, why it's even a question...",
"I would say yes, but I am not sure",
"I think no",
"Yea, of course",
"Sure it is, everyone knows it"
};

//when user starts sentence with can/could
char answer_can[][50] = {
"The only thing that I can't is move...",
"mmm, I could, but I have never tried",
"What if I can?",
"Now I doubt.",
"Can you?!?"
};

//When user enters stop in a sentence
char answer_stop[][50] = {
"Just do not tell me what to do.",
"Are you telling me to stop?",
"Should I really stop?",
"Will I actually stop this?",
"The whole fun is not to stop!"
};

#endif
