#include "define_variables.h"
#include "algorithms.h"

char *remove_char(char *input, char chr)
{
  int i, j=0;
  char new[size]; //temporary

  for(i=0; input[i] != '\0'; i++)
  {
    //If it is not that symbol, write it into temporary
    if(input[i] != chr)
    {
      new[j] = input[i];
      j++;
    }
  }
  j++;
  new[j-1] = '\0'; //End the string
  strcpy(input, new);
  return input;
}

void remove_spaces(char *given_string, char *result)
{
  int res_size;
  res_size=strlen(given_string);

  int j = 0, i, x; //for For loops

  //removing all spaces before sentence
  for(i=0; i<res_size; i++)
  {
    if(given_string[i]!=' ' || given_string[i+1]!=' ' )
    {
      if(given_string[i]==' ' &&given_string[i+1]=='\0')
      {
        result[j]='\0';
        break;
      }
      result[j]=given_string[i];
      j++;
    }

    if(i+1==res_size)
    {
      result[j]='\0';
    }
  }

  i=0;
  while(result[i]==' ')
  {
    for(x=0; x<strlen(result); x++)
    {
      result[x]=result[x+1];
    }
    result[x]='\0';
  }
}

void remove_insertion(char *input, const char *remove_string, char *result)
{
  strcpy(result, input);
  if(strstr(input, " ")==NULL)return;
  //char *temporary = (char*)malloc(strlen(input)*sizeof(char));

  char ignore[strlen(input)];
  lower_string(input);
  //strcpy(ignore, input);

  int i=0, x,m=0;

  //Only checking lowercase letters, because i did lower_string()
  if(starts_with(input, remove_string) && (input[strlen(remove_string)]<'a' || input[strlen(remove_string)]>'z'))
  {

    for(x=strlen(remove_string); x<strlen(input); x++)
    {
      result[m]=input[x];
      m++;
    }
    result[m] = '\0';

    strcpy(ignore, result);

    while((ignore[i] < 'a' || ignore[i] > 'z') && (ignore[i]!='\0' ||ignore[i]!='\n'))
    {
      i++;
    }
    m=0;
    for(x=i; x<strlen(ignore); x++)
    {
      result[m]=result[x];
      m++;
    }
    result[m]='\0';
  }
}

char *lower_string(char s[])
{
  int c = 0;

  while (s[c] != '\0')
  {
    //By ascii table
     if (s[c] >= 'A' && s[c] <= 'Z')
     {
        s[c] = s[c] + 32;
     }
      c++;
  }

  return s;
}
char *higher_string(char s[])
{
  int c = 0;

  while (s[c] != '\0')
  {
    //By ascii table
     if (s[c] >= 'a' && s[c] <= 'z')
     {
        s[c] = s[c] - 32;
     }
      c++;
  }

  return s;
}

void replace_word( char s[],  char old_word[], char newW[], char *result)
{
    int i, cnt = 0;
    int new_word_len = strlen(newW);
    int old_word_len = strlen(old_word);

    //Counting the number of times old_word occurrences
    for (i = 0; s[i] != '\0'; i++)
    {
      if((s[i-1]==' ' || s[i-1]=='\0') && s[i]==old_word[0] && (s[i+1]==' ' || s[i+1]=='?'))
        if (strstr(&s[i], old_word) == &s[i])
        {
            cnt++;
            //Moving to index after the old word.
            i += old_word_len - 1;
        }
    }

    i = 0;
    while (*s)
    {
        //compare the substring with the result
        if((s[0-1]==' ' || s[0-1]=='\0' || (s[0-1]<='a' || s[0-1]>='z') && (s[0-1]<='A' || s[0-1]>='Z'))
        && s[0]==old_word[0] &&
        (s[0+1]==' ' || s[0+1]=='?' || (s[0+1]<='a' || s[0+1]>='z') && (s[0+1]<='A' || s[0+1]>='Z')))
        {
          if (strstr(s, old_word) == s)
          {
            strcpy(&result[i], newW);
            i += new_word_len;
            s += old_word_len;
          }
        }
          else
            result[i++] = *s++;
    }

    result[i] = '\0';
    strcpy(s, result);
}

void abbreviations(char *result, char *new_result)
{
  char *resultt;
  int number_of_abbreviations=0;
  if(result==NULL)return;
  //result = new string without spaces
  resultt = (char *)malloc(strlen(result)*sizeof(char));
  //remove additional spaces if required and replace strings
  remove_spaces(result, resultt);
  strcpy(new_result, resultt);

  /*Removing these insertions in the beginning of a sentence,
  for a bot to be able to recognise sentences better*/
  remove_insertion(new_result, "anyways", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "for example", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "i mean", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "yea", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "however", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "ok", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "okay", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "yea", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "yes,", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "no,", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "so", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "so", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "nah", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "but", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "tbh", resultt);
  strcpy(new_result, resultt);
  remove_insertion(new_result, "lets say", resultt);
  strcpy(new_result, resultt);

  free(resultt);
  new_result[0] = tolower(new_result[0]);

  //what I am looking and what I will change to
  char u[] = "u";
  char r[] = "r";
  char k[] = "k";
  char ia[] = "i";
  char uu[] = "you";
  char rr[] = "are";
  char kk[] = "okay";
  char ii[] = "I";

  char space = ' ';
  char end = '\0';
  char sign1 = '.';
  char sign2 = '?';
  char sign3 = '!';

  int i=0;
  /*Now I am checking if abbreviation appears between spaces,
  or is a beginning of a sentence, or at the end*/
  while(new_result[i]!='\0')
  {
    if(
    new_result[i-1]==space || new_result[i-1]==end
    && (new_result[i] == u[0] || new_result[i] == r[0] || new_result[i] == k[0]
    || new_result[i] == ia[0])
    && ((new_result[i+1] ==space) || (new_result[i+1]== sign2)
    || (new_result[i+1] == sign1) || (new_result[i+1] == sign3)))
    {
      //Now I do not have to check the whole sentence again. Only to find the letter
      if(new_result[i]== u[0])
      {
        resultt = (char *)malloc(size*strlen(uu)*sizeof(char));
        replace_word(new_result, u, uu, resultt); //u->you
        strcpy(new_result, resultt);
        free(resultt);
      }
      else
      if (new_result[i]== k[0])
      {
        resultt = (char *)malloc(size*strlen(kk)*sizeof(char));
        replace_word(new_result, k, kk, resultt); //k->okay
        strcpy(new_result, resultt);
        free(resultt);
      }
      else
      if(new_result[i]== r[0])
      {
        resultt = (char *)malloc(size*strlen(rr)*sizeof(char));
        replace_word(new_result, r, rr, resultt); //r->are
        strcpy(new_result, resultt);
        free(resultt);
      }
      else
      if(new_result[i]== ia[0])
      {
        resultt = (char *)malloc(size*strlen(ii)*sizeof(char));
        replace_word(new_result, ia, ii, resultt); //i->I
        strcpy(new_result, resultt);
        free(resultt);
      }
      number_of_abbreviations++;
      if(number_of_abbreviations>10 && strlen(new_result)>50)//approximately
      {
        printf("Input is invalid\n");
        free(new_result);
        exit(0);
      }
    }
    i++;
  }

  //First character making upper
  new_result[0] = toupper(new_result[0]);
}

int similarity(char str1[], char str2[], int N)
{
  int i, longest, shortest;
  int number=0;
  char lon[size], sho[size];

  //ignore !?. when writing similarity
  remove_char(str1, '!');
  remove_char(str1, '?');
  remove_char(str1, '.');

  if(strlen(str1)>strlen(str2))
  {
    longest=strlen(str1);
    shortest=strlen(str2);
    strncpy(lon, str1, size);
    strncpy(sho, str2, size);
  }
  else
  {
    longest = strlen(str2);
    shortest=strlen(str1);
    strncpy(lon, str2, size);
    strncpy(sho, str1, size);
  }

  i=0;
  int x,w,s;

  lower_string(lon);
  lower_string(sho);

  while (sho[i]!='\0' && lon[i]!='\0')
  {
    //not changing, just checking
    if(sho[i]==lon[i])
    {
      number++;
      i++;
    }
    else if(sho[i]==' ')
    {
      while(lon[i]!=' ' && lon[i]!='\0')
      {
        w=0;
        //if its misspelled word, it still work
        for (s = 1; s <strlen(lon); s++)
            {
                lon[w]=lon[s];
                w++;
            }lon[strlen(lon)-1]='\0';//for making the last element null
      }
    }
    else if(lon[i]==' ')
    {
      while(sho[i]!=' ' && sho[i]!='\0')
      {
        w=0;
        //if its misspelled word, it still work
        for (s = 1; s <strlen(sho); s++)
            {
                sho[w]=sho[s];
                w++;
            }sho[strlen(sho)-1]='\0';//for making the last element null
      }
    }
    else ++i;

  }
  number=(number*100)/longest;

  QNA[N].percent=number;

  return number;
}

int find_the_number(int line_number, char *user_answer)
{
  int i;
  for(i=0; i<line_number; i++)
    similarity(user_answer, QNA[i].question, i);

  //Number equals the most similar answer line in the data file in any case
  return find_most_similar(user_answer, line_number);
}

int find_most_similar(char str[], int line)
{
  int number=QNA[0].nr;
  int percent=QNA[0].percent;
  int i;

  for(i=0; i<line; i++)
  {
    if(percent<QNA[i].percent)
    {
      percent=QNA[i].percent;
      number=i;
      i=QNA[i].nr;

      //For efficiency to break it instantly if it's the same
      if(percent == 100) break;
    }
  }
  return number;
}
