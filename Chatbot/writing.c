#include "define_variables.h"

void write_the_file(char *first, char *second, int line_number)
{
  FILE *closer;
  closer = fopen(data_file, "a+");

  //Making a digit
  char *string_number = (char*)malloc(size*sizeof(char));
  //Times three because of question/answer and any additional symbols
  char *with_dot = (char*)malloc((size*3)*sizeof(char));
  
  sprintf(string_number, "%d", line_number);
  int len = strlen(string_number);

  //Making single line out of strings
  strcpy(with_dot, string_number);
  strcat(with_dot, ".");
  strcat(with_dot, second);
  strcat(with_dot, "|");
  strcat(with_dot, first);
  strcat(with_dot, "\n");

  int i=0;
  while(i < strlen(with_dot))
  {
      fputc(with_dot[i], closer);
      i++;
  }
  free(string_number);
  free(with_dot);
  fclose(closer);
}
