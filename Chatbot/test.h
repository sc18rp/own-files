#ifndef test_h
#define test_h

void test_abbreviations(void);
void test_lower_higher_string(void);
void test_starts_with(void);
void test_swear_meet_happy_bye_words(void);
void test_append(void);
void test_replacing(void);
void test_english(void);
void test_remove_space(void);
void test_similarity(void);

int test_functions(void);

#endif
