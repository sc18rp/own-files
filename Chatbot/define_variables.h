#ifndef define_variables_h
#define define_variables_h

#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
//I put sleep() functions to look like a chatbot is thinking
//Also for dummy_print
#include "time.h"
#include "ctype.h"
#include "unistd.h"

//Because almost every module will use it
#include "dummy_print.h"

#define names "../text_files/names.txt"
#define en_words "../text_files/english_words.txt"
#define data_file "../text_files/data.txt"
#define size 100
#define Max_number_of_lines 10000
#define append_size 5

struct q_and_a{ //question and answer

  char question[100]; //not neccessarily a question
  char answer[100];
  int nr; //line number in a text file
  int percent; //this one is changing
};
struct q_and_a QNA[Max_number_of_lines];

#endif
