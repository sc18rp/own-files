#ifndef files_check_h
#define files_check_h

/*checks if sentence is written in english.
returns 1, 2, 3*/
int english_checker(char *input, int level_of_english);

/*checks if sentence is has name in it.*/
int name_checker(char *input);

#endif
