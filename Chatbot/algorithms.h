#ifndef algorithms_h
#define algorithms_h

//Removes any char given to this function
char *remove_char(char *strin, char chr);

//removes space before, after and in between string
void remove_spaces(char *given_string, char *result);

//Removes insertion, in the beginning of a sentence
void remove_insertion(char *input, const char *remove_string, char *result);

//Lowers/highers the whole string
char *lower_string(char s[]);
char *higher_string(char s[]);

//Replace word is used in abbreviations to swap the strings
void replace_word(char s[], char oldW[], char newW[], char *result);

//calculates all sentences similarities
int similarity(char str1[], char str2[], int N);

/*checks if there is an abbreviation and then calls the replaceWord
function with correct abbreviation given.
also abbreviations call other funcions which modifies the string*/
void abbreviations(char *result, char *new_result);

//Line number where most similar answer appeared
int find_the_number(int line_number, char user_answer[]);

//finds most similar out of database
int find_most_similar(char str[], int line);

#endif
