#include "define_variables.h"
#include "reading_file.h"
#include "algorithms.h"
#include "writing.h"
#include "files_check.h"


int chatbotmain(int argc, char *argv[])
{
  FILE *op;
  char user_input[size]; //user question
  char user_answer[size]; //user answer
  char *changed; //changed input
  int i, num;
  char option[8]; //For start/exit
  int value; //for checking bool values in algorithms
  int int_value; //for checking values required more that 2 options (english_checker)
  int line_number=0;
  //For mode in a chatbot
  int level_option;
  int level_of_english;
  int precision_of_a_sentance;

  //temporary opening the file to check if it exist
  op = fopen(data_file, "r");
  if(access(data_file, F_OK)==-1)
  {
    printf("File '%s' not found.\n", data_file);
    exit(0);
  }
  op = fopen(en_words, "r");
  if(access(en_words, F_OK)==-1)
  {
    printf("File '%s' not found.\n", en_words);
    exit(0);
  }
  if(access(names, F_OK)==-1)
  {
    printf("File '%s' not found.\n", names);
    exit(0);
  }
  //closing because it will be reopened later
  fclose(op);

  if(argc!=1)
  {
    printf("You should run a program file like this:\n");
    printf("./program_name\n");
    exit(0);
  }

  printf("\n\n********** I N S T R U C T I O N S **********\n");
  printf("**********HOW TO TALK WITH A DUMMY**********\n");
  printf("1.Avoid using swear words.\n");
  printf("2.Do not write nonsense, beacause the Chatbot will get dumber\n");
  printf("3.Talk in english\n");

  //For make reapeat if user enters not start or exit
  while(true)
  {
    printf("Write:\n" );
    printf(" '1' - to chat with a low precision, but answering more Dummy\n");
    printf(" '2' - to chat with a normal Dummy\n");
    printf(" '3' - to chat with a high precision, but less answering Dummy\n");

    if (scanf("%d", &level_option) == 0)
    {
      fputs("Char or string as an input is not supported\n", stderr);
      exit(0);
    }

    switch(level_option)
    {
      case 1:
      precision_of_a_sentance = 20;
      level_of_english = 50;
      break;

      case 2:
      precision_of_a_sentance = 70;
      level_of_english = 75;
      break;

      case 3:
      precision_of_a_sentance = 85;
      level_of_english = 90;
      break;
    }

    if(level_option!=1 && level_option!=2 && level_option!=3)
    printf("Only 1 or 2 or 3 please\n");
    else break;

  }
  printf("Now enter:\n");
  printf(" 'start' - to chat with a Dummy\n");
  printf(" 'exit' - to exit the chat (at any time)\n");
  printf("If you enter something else, you will get another chance to enter either 'start' or 'exit'\n");
  printf("P.S. These are not strict rules, but you should respect it\n" );
  printf("Enter your choice: ");

  getchar();
  scanf("%[^\t\n]s", option);

  lower_string(option);

  beginning: //goes there whenever it shouldnt reach the end of while loop
  //everytime, after a sentence is printed out, old user input is freed
  free(changed);
  while(true)
  {
    //simply read the file
    op = fopen(data_file, "r");
    reading_file(op, &line_number);
    if (op == NULL)
    {
        printf("Cannot open file\n");
        return 0;
    }

    //if a user press "start"
    value=0;
    printf("\n");
    if(strcmp(option, "start")==0)
    {
      //user enters a string
      printf("U: ");
      scanf(" %[^\t\n]s", user_input);
      sleep(1);
      if(strlen(user_input)>=size)
      {
        printf("Input too long.\n");
        //exits the program
        free(changed);
        exit(0);
      }
      if(strcmp(lower_string(user_input), "exit")==0)
      {
        free(changed);
        D_bye();
        exit(0);
      }
      //Allocating memory for 'changed'
      changed = (char *)malloc(sizeof(char)*size);
      //Check for abbrevations and change the string
      abbreviations(user_input, changed);

      //Looking for the most similar question if it exists
      for(i=0; i<line_number; i++)
        similarity(changed, QNA[i].question, i);

      //Number equals the most similar answer line in the data file
      num = find_most_similar(changed, line_number);

      //If the similarity of matching sentences is alike
      if(QNA[num].percent>=precision_of_a_sentance)
      {
        D_answer(QNA[num].answer);
      }
      else if(D_random_decision(changed))
      {
        /*Check for quick answer
        It is in the beginning Because
        I am not allowing swear words to be printed
        and Dummy has to say hi if user says it*/
        goto beginning;
      }

        //Its about to write it in the database, just has to do some checks first
        else if(QNA[num].percent<=precision_of_a_sentance)
        {
          /*Check if they all are english words or very similar
          Check if it can be rephrased*/
          int_value = english_checker(changed, level_of_english);
          if(name_checker(changed)) goto beginning;

          if(insta_starting(changed))goto beginning;
          if(int_value==2)
          {
            D_super_poor_en();
            goto beginning;
          }
          else if(int_value==1)
          {
            //checking if it is not one word or a phrase is to short
            if(D_short_phrase(changed))
            {
              goto beginning;
            }
            D_same(changed);
          }
          else if(int_value==0)
          {
            //output something about misspelled words
            D_poor_en();
            goto beginning;
          }

          printf("\nU: ");
          scanf(" %[^\t\n]s", user_answer);
          sleep(1);
          if(strlen(user_answer)>=size)
          {
            printf("Input too long.\n");
            free(changed);
            exit(0);
          }

          if(strcmp(lower_string(user_answer), "exit")==0)
          {
            free(changed);
            D_bye();
            exit(0);
          }

          //Applying 'abbrevations' in order to it, modifying it
          abbreviations(user_answer, user_answer);

          //Check if it contains a swear word
          if(swearing_options(user_answer))
          {
            //takes a swear_word function first and returns
            D_random_decision(user_answer);
            goto beginning;
          }
          //num = find_the_number(line_number, user_answer);
          for(i=0; i<line_number; i++)
            similarity(user_answer, QNA[i].question, i);

          //Number equals the most similar answer line in the data file
          num = find_most_similar(user_answer, line_number);

          if(QNA[num].percent>=precision_of_a_sentance)
          {
            D_answer(QNA[num].answer);
            write_the_file(user_answer, changed, line_number);
            line_number++;
            goto beginning;
          }
          else
          {
            if(name_checker(changed))goto beginning;

            if(insta_starting(user_answer))
            {
              //Writes insta starting and writes the sentence into a database
              write_the_file(user_answer, changed, line_number);
              line_number++;
              goto beginning;
            }
            if(!D_random_decision(user_answer))
              D_chatbot_lost();

            write_the_file(user_answer, changed, line_number);
            line_number++;
          }
        }
    }
    //if a user do not enter "start"
    else
    if(strcmp(option, "exit")!=0)
    {
      printf("Please enter again: ");
      scanf(" %[^\t\n]s", option);
      goto beginning;
    }
    else
    {
      free(changed);
      //Always write bye word then user exits
      D_bye();
      exit(0);
    }
  }

  return 0;
}
