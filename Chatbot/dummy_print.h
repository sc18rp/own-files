#ifndef dummy_print_h
#define dummy_print_h

/*Funcion, starting with D_ does just prints
the sentence, depending on that sentence
alse there is a random choice option in some functions*/

void D_name_isare(char *input);
void D_name(char *input);
void D_chatbot_lost(void); //to keep the conversation flowing after Dummy gets the answer.
void D_poor_en(void);
void D_super_poor_en(void);
void D_start_name(void);
void D_start_wow(void);
void D_start_are(void);
void D_start_is(void);
void D_start_can(void);
void D_start_stop(void);
void D_sure(void);
void D_math(void);
void D_bye(void);
void D_answer(char *input);
void D_name_terrible_is(char *name);
void D_name_terriblequestion(char *name);
void D_name_whyhow(char *input);
bool D_name_terrible(char *name, char *input);
void D_same(char *input);
bool D_random_decision(char input[]);
bool D_short_phrase(char *input);

//Just checks for negative word in a sentnce
bool negative_word_appears(char *input);

/*This function writes an insta starting if required.*/
bool insta_starting(char *input);
bool starts_with(const char *a, const char *b);

/*These funcions do not print anything,
they check if required string appears in a sentence*/
bool happy_options(char input[]);
bool swearing_options(char input[]);
bool meeting_options(char input[]);
bool bye_options(char input[]);

#endif
