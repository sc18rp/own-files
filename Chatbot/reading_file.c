#include "define_variables.h"

//appends the number
char *append(char *s, char c)
{
  //If number is too big
    if(strlen(s)>=append_size)
    {
      return NULL;
    }
    int len = strlen(s);
    s[len] = c;
    s[len+1] = '\0';
    return s;
}

void reading_file(FILE *reader, int *line_nru)
{
  int size_of_file;
  fseek(reader, 0, SEEK_END);
  size_of_file = ftell(reader);
  rewind(reader);
  //If file is empty
  if(size_of_file<2)
  {
    printf("Entered file is empty.\n" );
    exit(0);
  }
  char c;
  int tq; //temporary value for finding the number
  char number_string[append_size];
  int temp = 0;

  while(c!=EOF)
    {
      tq=0;
      number_string[0]='\0'; //everytime makes an empty string
      while(c!='|')//gets the question and nr until symbol '|'
      {
        if(c=='\n')
        {
          printf("File is not supported.\n");
          exit(0);
        }
        if(tq==0) //ignoring number
          {
            while(c!='.')
            {
              if(c!='.' && isdigit(c))//then it's a number 0-9
              {
                append(number_string, c);//gets number in char
              }

              c=getc(reader);
            }
          }

          while((c<='a' && c>='z') || (c<='A' && c>='Z'))
          {
            c=getc(reader);
          }

        //digit from string
        QNA[temp].nr=atoi(number_string);

        c=getc(reader); //first symbol

        if(c=='|')
          break;
        else
        {
          QNA[temp].question[tq]=c;
          tq++;
        }
      }

      tq=0;
      while(c!=EOF)
      {
        c=getc(reader);
        if(c=='\n')
        {
          break;
        }
        else if(c=='\0')
          break;

        QNA[temp].answer[tq]=c;
        tq++;
      }
      //Getting the last character
      c=getc(reader);

      //Initialising all struct array percentages to zero
      QNA[temp].percent=0;

      temp++;
  }

  *line_nru=temp;
  fclose(reader);
}
