#include "chatbotmain.h"
#include "test.h"

int main(int argc, char *argv[])
{
  //The main function which runs the chatbot
  chatbotmain(argc, argv);

  //Tests, checking if functions work fine
  test_functions();

  return 0;
}
