#include "unity.h"
#include "unity_internals.h"

#include "define_variables.h"
#include "algorithms.h"
#include "reading_file.h"
#include "files_check.h"

void test_abbreviations(void)
{
  char *changed, *to_be_tested;
  //Changed will be the changed string
  changed = (char*)malloc(sizeof(char)*size);

  to_be_tested = "How r u?";
  abbreviations(to_be_tested, changed);
  TEST_ASSERT_EQUAL_STRING("How are you?", changed);

  to_be_tested = "How are you?";
  abbreviations(to_be_tested, changed);
  TEST_ASSERT_EQUAL_STRING("How are you?", changed);

  to_be_tested = "r u k";
  abbreviations(to_be_tested, changed);
  TEST_ASSERT_EQUAL_STRING("Are you okay", changed);

  free(changed);
}

void test_lower_higher_string(void)
{
  char temp[size];
  //LOWER
  strcpy(temp, "heO"); //converting const char to char array
  TEST_ASSERT_EQUAL_STRING("heo", lower_string(temp));
  strcpy(temp, "OP ! aA");
  TEST_ASSERT_EQUAL_STRING("op ! aa", lower_string(temp));
  strcpy(temp, "_MmmM AAAXc");
  TEST_ASSERT_EQUAL_STRING("_mmmm aaaxc", lower_string(temp));

  //HIGHER
  strcpy(temp, "_MmmM AAAXc");
  TEST_ASSERT_EQUAL_STRING("_MMMM AAAXC", higher_string(temp));
  strcpy(temp, "HI");
  TEST_ASSERT_EQUAL_STRING("HI", higher_string(temp));
  strcpy(temp, " h ");
  TEST_ASSERT_EQUAL_STRING(" H ", higher_string(temp));
}


void test_swear_meet_happy_bye_words(void)
{
  //SWEAR
  TEST_ASSERT_TRUE(swearing_options("shit lol"));
  TEST_ASSERT_TRUE(swearing_options("SHIT"));
  TEST_ASSERT_TRUE(swearing_options("ShiT"));
  TEST_ASSERT_TRUE(swearing_options("Shit at the start"));
  TEST_ASSERT_TRUE(swearing_options("at the end: shiT"));
  TEST_ASSERT_FALSE(swearing_options("Good Sentance"));
  TEST_ASSERT_FALSE(swearing_options("-*`2134`3!@/?7"));
  TEST_ASSERT_FALSE(swearing_options("shomajo"));
  TEST_ASSERT_FALSE(swearing_options("sht"));
  TEST_ASSERT_FALSE(swearing_options(""));
  TEST_ASSERT_FALSE(swearing_options("0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"));

  //MEETING
  TEST_ASSERT_TRUE(meeting_options("Hi"));
  TEST_ASSERT_TRUE(meeting_options("helloooooo!"));
  TEST_ASSERT_FALSE(meeting_options("WYo"));
  TEST_ASSERT_FALSE(meeting_options("hdi"));
  TEST_ASSERT_FALSE(meeting_options("My name is Dummy"));
  TEST_ASSERT_FALSE(meeting_options("ArE you hiHIman? Yoo"));
  TEST_ASSERT_FALSE(meeting_options(" \n"));
  TEST_ASSERT_FALSE(meeting_options(""));
  TEST_ASSERT_FALSE(meeting_options(".........................................................................................................."));

  //HAPPY
  TEST_ASSERT_TRUE(happy_options("funny haha"));
  TEST_ASSERT_TRUE(happy_options("lmaoxd"));
  TEST_ASSERT_TRUE(happy_options(" XdDd "));
  TEST_ASSERT_FALSE(happy_options(""));
  TEST_ASSERT_FALSE(happy_options("ISSS is isis!"));
  TEST_ASSERT_FALSE(happy_options("haha                                                                                                     "));

  //Bye
  TEST_ASSERT_TRUE(bye_options("see ya"));
  TEST_ASSERT_TRUE(bye_options("yo man, peace out!"));
  TEST_ASSERT_FALSE(bye_options("yoyoyoyo  "));
}

void test_starts_with(void)
{ //True if insta start required
  TEST_ASSERT_TRUE(starts_with("Are you something?", "Are"));
  TEST_ASSERT_TRUE(starts_with("can you stop!", "can"));
  TEST_ASSERT_FALSE(starts_with("is it dark????\n", "IS"));
  TEST_ASSERT_TRUE(starts_with("       aRe som  ", "  "));
  TEST_ASSERT_TRUE(starts_with("ISSS is isis!", "ISS"));
  TEST_ASSERT_TRUE(starts_with("\n", ""));
}

void test_append(void)
{
  char string[append_size];
  string[0]='\0';
  TEST_ASSERT_EQUAL_STRING("H", append(string, 'H'));
  TEST_ASSERT_EQUAL_STRING("He", append(string, 'e'));
  TEST_ASSERT_EQUAL_STRING("HeY", append(string, 'Y'));
  TEST_ASSERT_EQUAL_STRING("HeY ", append(string, ' '));
  TEST_ASSERT_EQUAL_STRING("HeY m", append(string, 'm'));
  //because string is out of borders
  TEST_ASSERT_EQUAL_STRING(NULL, append(string, 'a'));
  TEST_ASSERT_EQUAL_STRING(NULL, append(string, 'n'));

}

void test_remove_char(void)
{
  char string[size] = "something!.??";
  TEST_ASSERT_EQUAL_STRING("something!.", remove_char(string, '?'));
  TEST_ASSERT_EQUAL_STRING("something!.", remove_char(string, 'X')); //Symbol does not exist
  TEST_ASSERT_EQUAL_STRING("omething!.", remove_char(string, 's'));
  TEST_ASSERT_EQUAL_STRING("omething!", remove_char(string, '.'));
}

void test_english(void)
{
  char *string = "something";
  TEST_ASSERT_EQUAL_INT(1, english_checker(string, 100));
  string = "what my name is?!!?";
  TEST_ASSERT_EQUAL_INT(1, english_checker(string, 100));
  string = "what my name isss?!!?";
  TEST_ASSERT_EQUAL_INT(0, english_checker(string, 100));
  string = "w your name is?!!?";
  TEST_ASSERT_EQUAL_INT(0, english_checker(string, 100));
  string = "awwaw housemaasrio aassasa";
  TEST_ASSERT_EQUAL_INT(2, english_checker(string, 100));
}

void test_remove_space(void)
{
  char *changed, *to_be_tested;
  //Changed is a string that is going to be changed
  changed = (char*)malloc(sizeof(char)*size);

  to_be_tested = "Expected";
  remove_spaces(to_be_tested, changed);
  TEST_ASSERT_EQUAL_STRING("Expected", changed);

  to_be_tested = "Expected   this";
  remove_spaces(to_be_tested, changed);
  TEST_ASSERT_EQUAL_STRING("Expected this", changed);

  to_be_tested = " Expected ";
  remove_spaces(to_be_tested, changed);
  TEST_ASSERT_EQUAL_STRING("Expected", changed);

  to_be_tested = "   Expected   this     ";
  remove_spaces(to_be_tested, changed);
  TEST_ASSERT_EQUAL_STRING("Expected this", changed);

  to_be_tested = "!--      =2                                                                                                                         ) ";
  remove_spaces(to_be_tested, changed);
  TEST_ASSERT_EQUAL_STRING("!-- =2 )", changed);

  free(changed);
}

void test_similarity(void)
{
  char original[size]="How old are you";
  int expected, size_of_temporary;
  int size_of_original = strlen(original);

  char temporary[size]="How old are yo";
  size_of_temporary = strlen(temporary);
  expected = size_of_temporary*100/size_of_original;
  TEST_ASSERT_INT_WITHIN(1, expected, similarity(original, temporary, 0));

  strcpy(temporary, "how ");
  size_of_temporary = strlen(temporary);
  expected = size_of_temporary*100/size_of_original;
  TEST_ASSERT_INT_WITHIN(1, expected, similarity(original, temporary, 0));

  strcpy(temporary, "h old are you");
  size_of_temporary = strlen(temporary);
  expected = size_of_temporary*100/size_of_original;
  TEST_ASSERT_INT_WITHIN(1, expected, similarity(original, temporary, 0));

  strcpy(temporary, "h o a y");
  size_of_temporary = strlen(temporary);
  expected = size_of_temporary*100/size_of_original;
  TEST_ASSERT_INT_WITHIN(1, expected, similarity(original, temporary, 0));

  strcpy(temporary, "");
  size_of_temporary = strlen(temporary);
  expected = size_of_temporary*100/size_of_original;
  TEST_ASSERT_INT_WITHIN(1, expected, similarity(original, temporary, 0));

  //Completely different
  strcpy(temporary, "Iamfine");
  TEST_ASSERT_INT_WITHIN(0, 0, similarity(original, temporary, 0));

  //3 spaces, 1-a, 3-you = 7matching; 7/28 = 25%
  strcpy(temporary, "Iamfineeeex what about youw ");
  TEST_ASSERT_INT_WITHIN(0, 25, similarity(original, temporary, 0));
}
void test_remove_insertion(void)
{
  char *changed;
  //Changed is a string that is going to be changed
  changed = (char*)malloc(sizeof(char)*size);

  remove_insertion("anyways, fine", "anyways", changed);
  TEST_ASSERT_EQUAL_STRING("fine", changed);

  remove_insertion("ok, , , , , i will", "ok", changed);
  TEST_ASSERT_EQUAL_STRING("i will", changed);

  remove_insertion("tbh.... , fine", "tbh", changed);
  TEST_ASSERT_EQUAL_STRING("fine", changed);

  free(changed);
}
void test_name_checker(void)
{
  //checking if it does not find unnecessary name in a sentence
  TEST_ASSERT_FALSE(name_checker("ROKAAS"));
  TEST_ASSERT_FALSE(name_checker("Rokazass i am rrokaS"));
  TEST_ASSERT_EQUAL(NULL, name_checker("I am roKas                             s                               s                                  s                               s"));
  TEST_ASSERT_FALSE(name_checker("rokasasaf"));
  TEST_ASSERT_FALSE(name_checker("roka s"));
  TEST_ASSERT_FALSE(name_checker("roka srokas"));
}
int test_functions(void)
{
  UNITY_BEGIN();

  RUN_TEST(test_abbreviations);

  //I put them both in the same one, because they work in the same principle
  RUN_TEST(test_lower_higher_string);

  //3 in 1 because they are all working similar
  RUN_TEST(test_swear_meet_happy_bye_words);

  RUN_TEST(test_starts_with);
  RUN_TEST(test_append);
  RUN_TEST(test_remove_char);

  //This one takes a while, because it has to scan every english word
  RUN_TEST(test_english);
  RUN_TEST(test_remove_space);
  RUN_TEST(test_similarity);
  RUN_TEST(test_remove_insertion);
  RUN_TEST(test_name_checker);

  return UNITY_END();
}
