import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.StackPane;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;


public class AnimatedGame extends Application {

    @Override public void start(Stage stage)throws IllegalArgumentException {

      //For buttons and cards
      TilePane root = new TilePane();
      //I am adding buttons and labels in the grid
      GridPane grid = new GridPane();

      //Making a deck and shuffeling the cards
      BlackjackHand hand = new BlackjackHand();
      Deck deck = new Deck();
      deck.shuffle();

      Button startButton = new Button("Hit card");
      Button endButton = new Button("Stand");
      startButton.setText("Hit");
      endButton.setText("Stand");

      //HIT button
      startButton.setOnAction(e ->
        {
          try{

          Card newone = new Card(deck.deal().toString());
          hand.add(newone);
          int thesize = hand.toString().length();
          System.out.println(newone);
          FancyCard card = new FancyCard(newone.toString());

          root.getChildren().add(new ImageView(card.getImage()));

          Label label = new Label();
          label.setTextFill(Color.LIGHTBLUE);
          //Adding the label to the grid, to be printed out
          grid.add(label, 300, 200);
          if(hand.isBust())
          {
            label.setText("You have been busted!");
            grid.add(label, 250, 750);
            //Creating new scene, because I am exiting the system
            Scene scenes = new Scene(grid, 640, 400);
            stage.setScene(scenes);
            stage.setWidth(800);
            stage.setHeight(600);
            stage.show();
          }
          }
          catch(Exception ee)
          {
            System.out.println("You have been busted");
          }
        }
      );

      //STAND button
      endButton.setOnAction(e ->
        {
          try{
            Label label1 = new Label();
            label1.setMinWidth(100);
            label1.setMinHeight(100);
            label1.setText("You Standed with value: " + hand.value());

            label1.setTextFill(Color.LIGHTBLUE);

            grid.add(label1, 500, 750);
            Scene scenes = new Scene(grid, 640, 400, Color.DARKGREEN);
            stage.setScene(scenes);
            stage.setWidth(800);
            stage.setHeight(600);
            stage.show();
            System.out.println("You got:" + hand.value());
          }
          catch(Exception ee){
            System.out.println("Something went wrong, interruption appeared.");
          }
        }
      );
      root.getChildren().add(startButton);
      root.getChildren().add(endButton);

      //Adding all the elements to a grid
      grid.add(startButton, 150, 150);
      grid.add(root, 250, 500);
      grid.add(endButton, 250, 150);

      grid.setStyle("-fx-background-color: #006600");

      //Adding the whole grid to the scene full of elements added into
      Scene scenes = new Scene(grid, 640, 400, Color.DARKGREEN);
      stage.setScene(scenes);
      stage.setWidth(800);
      stage.setHeight(600);

      stage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
  }
