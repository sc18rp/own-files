import java.util.ArrayList;
import java.util.Collections;

class Deck extends CardCollection{

  //The default constructor
  public Deck(){

    super(); //invokes the superclass

    for(Card.Suit suits : Card.Suit.values())
    {
      for(Card.Rank ranks : Card.Rank.values())
      {
        Card card = new Card(ranks, suits); //creates a card object
        cards.add(card); //adds a new card
      }
    }
  }

  public void shuffle(){
    Collections.shuffle(cards);
  }

  public Card deal(){ //returns
    if(cards.isEmpty()==true)
    {
      throw new CardException("The deck is empty");
    }

    Card topCard = cards.get(0);
    cards.remove(topCard);

    return topCard;
  }

}
