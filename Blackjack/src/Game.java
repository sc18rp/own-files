import java.util.Arrays;
import java.io.*;
import java.util.Scanner;
import java.util.*;
import java.util.concurrent.TimeUnit;

class Game
{
  public static void main(String[] args)throws InputMismatchException, InterruptedException
  {
    Card.fancySymbols = true;
    Scanner secondOption = new Scanner(System.in);
    Scanner firstOption = new Scanner(System.in);

    BlackjackHand playerHand = new BlackjackHand();
    BlackjackHand dealerHand = new BlackjackHand();

    int playerWon = 0, dealerWon = 0, aTie = 0, roundsPlayed = 0; //For statistics
    boolean value = true; //for Hit and Stand
    boolean repeat_value; //For repeating the 'H' or 'S' when entered wrong letter

    while(true)
    {
      System.out.printf("Next [R]ound or [Q]uit? ");

      //Making both upper and lower case letters be accepted
      switch(firstOption.nextLine().toUpperCase())
      {
        case "R":
            roundsPlayed++;
            playerHand.discard();
            dealerHand.discard();
            //Creating a new deck and then shuffleing it
            Deck theDeck = new Deck();
            theDeck.shuffle();

            value = true; //For making firstOption repeat
            while(value)
            {
              //To proceed the game better and more interesting
              TimeUnit.MILLISECONDS.sleep(500);
              playerHand.add(theDeck.deal());
              //I am showing the value, because it is more convenient
              System.out.println("\n" + playerHand + " ("+ playerHand.value() +")");

              if(playerHand.isBust())
              {
                System.out.println("Your hand is busted.");
                System.out.println("---Dealer wins!---\n");
                dealerWon++;
                break;
              }
              if(playerHand.isNatural())
              {
                System.out.println("You got a BlackJack!");
                value = false;
              }
              /*If a player gets 21, he does not have to pick more,
                because there is no point*/
              else if(playerHand.value()==21)
              {
                System.out.println("Wow! Now lets wait for the dealer.\n");
                value = false;
              }
              else System.out.printf("[H]it or [S]tand? ");

              repeat_value = true;
              if(value) // If it is not natural and not 21
              while(repeat_value)//to make repeat if missclicked
              switch(secondOption.nextLine().toUpperCase())
              {
                case "H": value = true; repeat_value = false; break;
                case "S": value = false; repeat_value = false; break;
                default: System.out.printf("\nOnly 'H' or 'S' please: ");
                          break;
              }

              //If a player hits 's' or gets a 21
              if(!value)
              {
                System.out.printf("\nNow dealer's turn: \n");
                value=true;
                while(value)
                {
                  //Making dealer cards appear slow, because it is more fun
                  TimeUnit.SECONDS.sleep(1);
                  dealerHand.add(theDeck.deal());
                  //Showing dealer value
                  System.out.println(dealerHand + " ("+ dealerHand.value() +")");
                  if(dealerHand.value()>=17)
                  {
                    //Making the value false, because dealer cant pick up more cards
                    value = false;
                  }
                  if(dealerHand.isBust())
                  {
                    System.out.println("Dealer's hand is busted.");
                    System.out.println("---Player wins!---\n");
                    playerWon++;
                    value = false;
                    break;
                  }
                  if(dealerHand.isNatural())
                  {
                    System.out.println("Dealer has a BlackJack!");
                    //Does not count point to dealer yet, because player may have got Natural as well
                    value = false;
                    break;
                  }
                }
              }
              //Just to be sure the deck is not empty
              if(theDeck.isEmpty())
              {
                System.out.println("The deck of cards is empty.");
                System.exit(0);
              }
            }
        if(dealerHand.value()>playerHand.value() && !dealerHand.isBust())
        {
          System.out.println("---Dealer wins!---\n");
          dealerWon++;
        }
        else if(dealerHand.value()<playerHand.value() && !playerHand.isBust())
        {
          System.out.println("---Player wins!---\n");
          playerWon++;
        }
        else if(dealerHand.value() == playerHand.value() && !playerHand.isBust() && !dealerHand.isBust())
        {
          /*This is if One of us get Natural and another get simple 21,
            But not natural, then Natural wins*/
          if(playerHand.isNatural() && !dealerHand.isNatural())
          {
            System.out.println("---Player wins!---\n");
            playerWon++;
          }
          else if(!playerHand.isNatural() && dealerHand.isNatural())
          {
            System.out.println("---Dealer wins!---\n");
            dealerWon++;
          }
          else
          {
            //Only if both are natural or score is equal.
            System.out.println("---It's a tie!---\n");
            aTie++;
          }
        }
        break;

        //Print all the statistics when 'Q'
        case "Q":
                System.out.println("Rounds played: " + roundsPlayed);
                System.out.println("Rounds Player won: " + playerWon);
                System.out.println("Rounds Dealer won: " + dealerWon);
                System.out.println("Tie played rounds: " + aTie);
                System.exit(0);

        default: System.out.println("Only 'R' or 'Q' please!");

      }
    }
  }
}
