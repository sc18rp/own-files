import java.util.Arrays;
import java.io.*;
import java.util.StringJoiner;

class BlackjackHand extends CardCollection{
  //The default constructor
    public static boolean fancySymbols = false;
  public BlackjackHand()
  {
    //contains no cards
    super();
  }

  public BlackjackHand(String cards)
  {
    if(cards!=null)
    {
      //split the string
      String[] splitted = cards.split("\\s+");

      int i;
      for(i=0; i<(cards.length()+1)/3; i++)
      {
        this.cards.add(new Card(splitted[i]));
      }
    }
  }
  public void discard(Deck deck){
    if(cards.isEmpty())
    {
      throw new CardException("The deck is empty");
    }

    int i = cards.size();
    while(!cards.isEmpty())
    {
      //Adds the last card to the deck
      deck.add(cards.get(i-1));
      //Then removes that card
      cards.remove(i-1);
      i--;
    }
  }

  @Override
  public String toString(){

    StringJoiner string = new StringJoiner(" ");
    for(int i=0; i<cards.size(); i++)
    {
      string.add(cards.get(i).toString());
    }

    return string.toString();
  }

  public void add(Card card) {
    if(BlackjackHand.this.isBust())
    {
      throw new CardException("Hand is busted");
    }
    cards.add(card);
  }

  public int value()
  {
    int sum = 0;
    int aces = 0;
    int x;
    //Checking each card
    for (Card card: cards)
    {
      if(card.getRank() == Card.Rank.ACE)
      {
        //Ace found
        aces += 1;
      }
      else if(card.getRank() != Card.Rank.ACE)
        sum += card.value();
    }

        if((sum+11+aces-1)<=21 && aces!=0)
        {
          sum+=11+aces-1; //minus one because one ace is added - "11"
        }
        else sum = sum + aces; //Because 1Ace = 1Score

    return sum;
  }

  public boolean isNatural() //21 in two cards
  {
    boolean value=false;
    int sum = 0;
    if(cards.size() == 2)
    {
      //(Ace and value ten) or (value ten and ace)
      if((cards.get(0).getRank() == Card.Rank.ACE && cards.get(1).value() == 10)
      ||
      (cards.get(1).getRank() == Card.Rank.ACE && cards.get(0).value() == 10))
      {
        value = true;
      }
    }

    return value;
  }
  public boolean isBust() //excedes 21 - busted
  {
    boolean value = false;

    int sum = 0;
    for(int i = 0; i<cards.size(); i++)
    {
      sum += cards.get(i).value();
    }
    if(sum>21) value = true;

    return value;
  }

}
