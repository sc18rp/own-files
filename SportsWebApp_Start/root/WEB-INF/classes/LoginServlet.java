// javac -cp /home/csunix/sc18ir/Desktop/jetty/team30/lib/servlet-api-3.1.jar LoginServlet.java

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseManager;

public class LoginServlet extends HttpServlet {



	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("login.jsp").forward(request,response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		DatabaseManager database = new DatabaseManager();

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		// check if details are correct with database
		if(database.match(username,password)){
			request.getSession().setAttribute("user", username);
		}

		response.sendRedirect("/");
	}
}
