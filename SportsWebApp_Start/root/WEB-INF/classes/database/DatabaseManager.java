// javac -cp /home/csunix/sc18rb/Desktop/jetty/team30/lib/sql2o-1.6.0.jar Account.java DatabaseManager.java
package database;

import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;

import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import org.mindrot.jbcrypt.BCrypt;

public class DatabaseManager {

    private Sql2o db;

    public DatabaseManager() {

        //establish connection with our db
        db = new Sql2o("jdbc:sqlite:accounts.db","","");

    }

    /*
     * Deletes users record from the database
     * RETURNS true if successful, false if connections with the database failed.
     */
    public boolean delete(String email) {

        if (!userExists(email)) {

            throw new IllegalArgumentException("Can't delete non-existent user");

        } else {

            try (Connection t = db.open()) {

                String insertQuery = "DELETE FROM accounts WHERE email = :u";
                Query query = t.createQuery(insertQuery).addParameter("u", email);
                query.executeUpdate();

            } catch (Exception ex) {

                System.err.println("Can't delete user : " + ex.getMessage());
                return false;

            }

        }

        return true;
    }

    /*
     * Adds user record to the database
     * RETURNS true if successful, false if connections with the database failed.
     */
    public boolean register(String email, String firstname, String lastname, String password, String role) {

        if (userExists(email)) return false;

        try (Connection t = db.open()) {

            String insertQuery = "INSERT INTO accounts (email, firstname, lastname, password, role) VALUES (:email,:fname, :lname, :password, :role)";
            String passwordHash = BCrypt.hashpw(password, BCrypt.gensalt());
            Query query = t.createQuery(insertQuery).addParameter("email", email).addParameter("fname", firstname).
            addParameter("lname", lastname).addParameter("password", passwordHash).addParameter("role", role);
            query.executeUpdate();

        } catch (Exception ex) {

            System.err.println("Can't register user : " + ex.getMessage());
            return false;

        }

        return true;

    }

    /*
     * RETURNS a list of all emails in the database
     */
    public List<String> getAllUsers () {

        List<Account> users;

        try (Connection t = db.open()) {

            String getQuery = "SELECT * FROM accounts";
            Query query = t.createQuery(getQuery);
            users = query.executeAndFetch(Account.class);

        }

        if (users == null) return null;
        else {
            List<String> emails = new LinkedList<>();
            for (int i = 0; i < users.size();i++) {
                emails.add(users.get(i).getEmail());
            }

            return emails;
        }
    }

    /*
     * RETURNS true if user exists in the database
     */
    public boolean userExists(String email) {

        if(getAllUsers().contains(email)) return true;
        else return false;

    }

    /*
     * RETURNS true if there exists user with the provided email AND this email is
     * associated with password.
     */
     public boolean match(String email, String password) {

             List<Account> account;
             try (Connection t = db.open()) {

                 String getPassQuery = "SELECT password FROM accounts WHERE email = :c";
                 Query passQuery = t.createQuery(getPassQuery).addParameter("c",email);
                 List <Account> storedPasswordHashList = passQuery.executeAndFetch(Account.class);
                 Account storedPasswordHashAccount= storedPasswordHashList.get(0);
                 String storedPasswordHash = storedPasswordHashAccount.getPassword();
                 if(BCrypt.checkpw(password, storedPasswordHash))
                 {
                   String getQuery = "SELECT * FROM accounts WHERE email = :u AND password = :p";
                   Query query = t.createQuery(getQuery).addParameter("u",email).addParameter("p",storedPasswordHash);
                   account = query.executeAndFetch(Account.class);
                   return true;
                 }
                 else return false;
             }
         }


     public boolean changeRole(String email, String newrole) {

           try(Connection t = db.open()){

               String getQuery = "UPDATE accounts SET role = :role WHERE email = :email";
               Query query = t.createQuery(getQuery).addParameter("role", newrole).addParameter("email", email);
               query.executeUpdate();

               return true;
           }
           catch (Exception ex) {

               System.err.println("No account found for given email " + ex.getMessage());
               return false;

           }
     }

     public String getRole(String email){


       try(Connection t = db.open()){

           String getQuery = "SELECT role FROM accounts WHERE email = :c";
           Query query = t.createQuery(getQuery).addParameter("c", email);
           List <Account> accounts = query.executeAndFetch(Account.class);
           Account acc = accounts.get(0);
           return acc.getRole();
       }
       catch (Exception ex) {
           return null;

       }
     }

     // ------------- bankAccounts Table Methods -------------

     // Add and Delete
     public boolean addCard(String email, String cardNumber, String cardName, int expiryDate, int cvv){

       try (Connection t = db.open()) {

         String getQuery = "INSERT INTO BankAccount (cardNumber, cardName, expiryDate, cvv, email) VALUES(:cardNumber, :cardName, :expiryDate,:cvv, :email)";
         Query query = t.createQuery(getQuery).addParameter("cardNumber", cardNumber).addParameter("cardName", cardName)
         .addParameter("expiryDate", expiryDate).addParameter("cvv", cvv).addParameter("email", email);

         query.executeUpdate();
         return true;

       }
       catch(Exception ex){
         System.err.println("" + ex.getMessage());
         return false;
       }
     }

    public boolean deleteCard(String email, String cardNumber) {

         if (!userExists(email)) {
           System.out.println(email);
             throw new IllegalArgumentException("Can't delete non-existent user");

         } else {

             try (Connection t = db.open()) {

                 String insertQuery = "DELETE FROM BankAccount WHERE cardNumber = :cardNumber";
                 Query query = t.createQuery(insertQuery).addParameter("cardNumber", cardNumber);
                 query.executeUpdate();

             } catch (Exception ex) {

                 System.err.println("Can't delete bank acocunt : " + ex.getMessage());
                 return false;
             }

         }

         return true;
     }

 }
