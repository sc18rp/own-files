package database;

public class Account {

    private String firstName;
    private String lastName;
    private String email;
    private String role;
    private String password;

    public String getEmail() {
        return email;
    }

    public String getPassword(){
      return password;
    }

    public String getRole(){
      return role;
    }
}
