import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import database.DatabaseManager;

public class ReceiptServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    String username = "";

    if( request.getSession().getAttribute("user") != null){
      username =  request.getSession().getAttribute("user").toString();
    }

    DatabaseManager db = new DatabaseManager();

    if(db.getRole(username) != null && "employee".equals(db.getRole(username))){
      request.getRequestDispatcher("employeepages/receipt.jsp").forward(request,response);
    }
    else{
      response.sendRedirect("/");
    }
  }

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String email = request.getParameter("email");
		String product = request.getParameter("product");
		int amount = Integer.parseInt(request.getParameter("amount"));

    System.out.println("Receipt sent to:");
    System.out.println(email);


    response.sendRedirect("/");
	}
}
