<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<title>My Web App</title>
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="assets/fonts/flat-icon/flaticon.css">
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="assets/css/slicknav.min.css" type="text/css">
		<link rel="stylesheet" href="assets/css/themify-icons.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aboutus.css">
	</head>
	<body>

		<!-- Header Area Starts -->
		<header>
		    <nav>
		      <div id="navbar">
		        <div id="logo" class="reverse">
							<div class="mobile-btn" style="font-size:30px;cursor:pointer; font-weight:bold;" onclick="openNav()">&#9776;</div>
		          <div class="logo">Team30 <span>GYM</span></div>
		        </div>
		        <div id="links">
              <a href="index.jsp">Home</a>
		          <a href="aboutus.jsp">About Us</a>
		          <a href="schedule.jsp">Memberships</a>
		          <a href="">Trainers</a>
							<a href="">Contact</a>
		          <a href="login.jsp">Login</a>
		        </div>
		      </div>

		    </nav>
		    <!-- Mobile Menu -->
		    <div id="mySidenav" class="sidenav">
		      <a style="cursor:pointer;" class="closebtn" onclick="closeNav()">&times;</a>
          <a href="index.jsp">Home</a>
          <a href="aboutus.jsp">About Us</a>
          <a href="schedule.jsp">Memberships</a>
          <a href="">Trainers</a>
          <a href="">Contact</a>
          <a href="login.jsp">Login</a>
		    </div>
		  </header>
		<!-- Header Area End -->


		<!-- Body Area Start -->
    <div class="section">
      <div class = "grid">
        <h1><center><b>About Us</b><center></h1>
        </br>
        <h2><center>Our clubs offer members of all ages the opportunity to exercise and live a healthy lifestyle in a non-intimidating and judgement-free environment, at an affordable cost.
          </center></h2>
          </br>

Our aim is to offer the best value for money in the industry. We love it when our customers tell us that they can’t believe the facilities that they get for the price. We are a value brand that has designed its business around passing cost savings on to our customers.
Customer Service

We are a value brand, but we will not compromise on our customer service. We pride ourselves in giving the best customer service to all our members with fully staffed facilities, and a full class timetable, which is unique in our sector of the market. You will always get a smile from our team. We work very closely with all our members, and ensure we help them in any way possible. We offer everything from non-contract memberships, to the ability of being able to freeze your account when desirable, to ensure we can fit in with your lifestyle.

These core values drive us to create unbeatable customer service that gives our members the opportunity and the tools to change their lives.


What makes us different?

We pride ourselves in the facilities that we have on offer throughout our fitness clubs.

With various sites offering a full boxing ring, combat classes and TRX facilities, and with over 400 pieces of equipment at all of our clubs, we offer the best all round member experience in the industry compared to other budget operators. At the majority of our sites, we have extensive car parking for all members to use free of charge.

The sizes of our clubs are designed to make you feel at ease even through the busiest times, allowing you to stick to your exercise plan and not have to wait for any equipment. Our clubs are designed especially with customers in mind, fully air conditioned and have changing room and shower facilities with secure lockers for you to use at your leisure.

Throughout all our fitness clubs, we offer all members up to 40 free classes per week, included in your monthly membership, with a class for everyone of all ages. Not only this, all our members have the opportunity to utilise our team of Personal Trainers which are on hand to help everyone to achieve their goals.

      </div>
    </div>
		<!-- Body Area End -->




		<!-- Footer Area Starts -->
		<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>About</h6>
            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul class="footer-links">
              <li><a href="http://scanfcode.com/about/">About Us</a></li>
              <li><a href="http://scanfcode.com/contribute-at-scanfcode/">Trainers</a></li>
              <li><a href="http://scanfcode.com/privacy-policy/">Privacy Policy</a></li>
              <li><a href="http://scanfcode.com/sitemap/">Contact</a></li>
            </ul>
          </div>
					<div class="col-xs-6 col-md-3">
								<div class="single-widget-home">
										<h6>Newsletter</h6>
										<p class="mb-4">Enter your email to get a newsletter every month!</p>
										<form action="#">
												<input type="email" placeholder="Your email here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email here'" required>
												<button type="submit" class="template-btn"><i class="fa fa-long-arrow-right"></i></button>
										</form>
								</div>
					</div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a class="linkedin" href="#"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
</footer>
<!-- Footer Area Ends -->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/mixitup.min.js"></script>
<script src="assets/js/jquery.slicknav.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/nav.js"></script>
	</body>
</html>
