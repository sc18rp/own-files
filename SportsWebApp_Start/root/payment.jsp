<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<title>My Web App</title>
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="assets/fonts/flat-icon/flaticon.css">
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="assets/css/slicknav.min.css" type="text/css">
		<link rel="stylesheet" href="assets/css/themify-icons.css" type="text/css">
		<link rel="stylesheet" href="assets/css/login.css" type="text/css">
	</head>

  <body>
<!-- Body Area Start -->
<div class="wrapper">
    <div class="sct brand"></div>
    <div class="sct login">

        <form method="POST" action="/payment">
            <h3>Payment</h3>
          </br>
            <input type="text" name="cardnumber" placeholder="Card Number">
            <input type="text" name="cardname" placeholder="Card Name">
            <input type="number" name="cvv" placeholder="CVV">
            <input type="number" name="exp" placeholder="Expiry Date">
            <input type="submit" name="send" value="Pay">
        </form>

    </div> <!--end login-->
</div> <!--end wrapper-->
<!-- Body Area End -->
</body>
