<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<title>My Web App</title>
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="assets/fonts/flat-icon/flaticon.css">
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="assets/css/slicknav.min.css" type="text/css">
		<link rel="stylesheet" href="assets/css/themify-icons.css" type="text/css">
	</head>
	<body>

		<!-- Header Area Starts -->
		<header>
		    <nav>
		      <div id="navbar">
		        <div id="logo" class="reverse">
							<div class="mobile-btn" style="font-size:30px;cursor:pointer; font-weight:bold;" onclick="openNav()">&#9776;</div>
		          <div class="logo">Team30 <span>GYM</span></div>
		        </div>
		        <div id="links">
							<a href="index.jsp">Home</a>
		          <a href="aboutus.jsp">About Us</a>
		          <a href="schedule.jsp">Memberships</a>
		          <a href="">Trainers</a>
							<a href="">Contact</a>
		          <a href="login.jsp">Login</a>
		        </div>
		      </div>

		    </nav>
		    <!-- Mobile Menu -->
		    <div id="mySidenav" class="sidenav">
		      <a style="cursor:pointer;" class="closebtn" onclick="closeNav()">&times;</a>
					<a href="index.jsp">Home</a>
					<a href="aboutus.jsp">About Us</a>
					<a href="schedule.jsp">Memberships</a>
					<a href="">Trainers</a>
					<a href="">Contact</a>
					<a href="login.jsp">Login</a>
		    </div>
		  </header>
		<!-- Header Area End -->


		<!-- Body Area Start -->
		<!-- Body Area End -->




		<!-- Footer Area Starts -->
		<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>About</h6>
            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul class="footer-links">
              <li><a href="http://scanfcode.com/about/">About Us</a></li>
              <li><a href="http://scanfcode.com/contribute-at-scanfcode/">Trainers</a></li>
              <li><a href="http://scanfcode.com/privacy-policy/">Privacy Policy</a></li>
              <li><a href="http://scanfcode.com/sitemap/">Contact</a></li>
            </ul>
          </div>
					<div class="col-xs-6 col-md-3">
								<div class="single-widget-home">
										<h6>Newsletter</h6>
										<p class="mb-4">Enter your email to get a newsletter every month!</p>
										<form action="#">
												<input type="email" placeholder="Your email here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email here'" required>
												<button type="submit" class="template-btn"><i class="fa fa-long-arrow-right"></i></button>
										</form>
								</div>
					</div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a class="linkedin" href="#"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
</footer>
<!-- Footer Area Ends -->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/mixitup.min.js"></script>
<script src="assets/js/jquery.slicknav.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/nav.js"></script>
	</body>
</html>
