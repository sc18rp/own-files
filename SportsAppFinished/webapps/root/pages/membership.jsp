<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<title>TEAM30 GYM</title>
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="../assets/fonts/flat-icon/flaticon.css">
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="../assets/css/style.css">
		<link rel="stylesheet" href="../assets/css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/themify-icons.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/membership.css" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;600;700&display=swap" rel="stylesheet">
    </head>
    <body>
        <!-- Nav Area Starts -->
        <header>
            <nav>
                <div id="navbar">
                    <div id="logo" class="reverse">
                        <div class="mobile-btn" style="font-size:30px;cursor:pointer; font-weight:bold;" onclick="openNav()">&#9776;</div>
                        <div class="logo">Team30 <span>GYM</span></div>
                    </div>
                    <div id="links">
                        <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                        <a href="/receipt">Send Receipts</a>
                        <% } %>
                        <a href="/">Home</a>
                        <a href="/about">About Us</a>
                        <% if(request.getSession().getAttribute("membership") != null){ %>
                        <a href="/displayall">Activities</a>
                        <% } else{ %>
                        <a href="/memberships">Memberships</a>
                        <% } %>
												
                        <a href="/facilities">Facilities</a>
                        <% if(request.getSession().getAttribute("user") != null){ %>
                        <a href="/userbookings">My Bookings</a>
                        <a href="/logout">Logout</a>
                        <% } else{ %>
                        <a href="/login">Login</a>
                        <% } %>
                    </div>
                </div>
            </nav>
            <!-- Mobile Menu -->
            <div id="mySidenav" class="sidenav">
                <a style="cursor:pointer;" class="closebtn" onclick="closeNav()">&times;</a>
                <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                <a href="/receipt">Send Receipts</a>
                <% } %>
                <a href="/">Home</a>
                <a href="/about">About Us</a>
                <% if(request.getSession().getAttribute("membership") != null){ %>
                <a href="/displayall">Activities</a>
                <% } else{ %>
                <a href="/memberships">Memberships</a>
                <% } %>
								
                <a href="/facilities">Facilities</a>
                <% if(request.getSession().getAttribute("user") != null){ %>
                <a href="/userbookings">My Bookings</a>
                <a href="/logout">Logout</a>
                <% } else{ %>
                <a href="/login">Login</a>
                <% } %>
            </div>
        </header>
        <!-- Nav Area Ends -->

        <!-- Content Area Starts -->
        <div id="content-area">
            <div id="header-area">
                <h1 id="page-header">Membership Options</h1>
            </div>

            <div class="container">
                <div class="row">
                    <!-- First Column -->
                    <div class="col-lg-4">
                        <div class="generic_content clearfix">
                            <div class="generic_head_price clearfix">
                                <div class="generic_head_content clearfix">
                                    <div class="head_bg"></div>
                                    <div class="head">
                                        <span>Basic</span>
                                    </div>
                                </div>
                                <div class="generic_price_tag clearfix">
                                    <span class="price">
                                        <span class="sign">&#163;</span>
                                        <span class="currency">9</span>
                                        <span class="cent">.99</span>
                                        <span class="month">/MON</span>
                                    </span>
                                </div>
                            </div>
                            <div class="generic_feature_list">
                                <ul>
                                    <li><span>Limited Facilities Access</span></li>
                                    <li><span>Off Peak Access</span></li>
                                    <li><span>24/7 Support</span></li>
                                    <li><span></span>-</li>
                                    <li><span></span>-</li>
                                </ul>
                            </div>
                            <div class="generic_price_btn clearfix">
                                <a class="" href="/buy">Buy</a>
                            </div>
                        </div>
                    </div>

                    <!-- 2nd Column -->
                    <div class="col-lg-4">
                        <div class="generic_content active clearfix">
                            <div class="generic_head_price clearfix">
                                <div class="generic_head_content clearfix">
                                    <div class="head_bg"></div>
                                    <div class="head">
                                        <span>Premium</span>
                                    </div>
                                </div>
                                <div class="generic_price_tag clearfix">
                                    <span class="price">
                                        <span class="sign">&#163;</span>
                                        <span class="currency">19</span>
                                        <span class="cent">.99</span>
                                        <span class="month">/MON</span>
                                    </span>
                                </div>
                            </div>
                            <div class="generic_feature_list">
                                <ul>
                                    <li><span>Full Access to Facilities</span></li>
                                    <li><span>Anytime Access to Most Facilities</span></li>
                                    <li><span>1 Free Body Analysis Per Month</span></li>
                                    <li><span>24/7 Support</span></li>
                                    <li><span></span>-</li>
                                </ul>
                            </div>
                            <div class="generic_price_btn clearfix">
                                <a class="" href="/buy">Buy</a>
                            </div>
                        </div>
                    </div>

                    <!-- 3rd Column -->
                    <div class="col-lg-4">
                        <div class="generic_content clearfix">
                            <div class="generic_head_price clearfix">
                                <div class="generic_head_content clearfix">
                                    <div class="head_bg"></div>
                                    <div class="head">
                                        <span>Platinum</span>
                                    </div>
                                </div>
                                <div class="generic_price_tag clearfix">
                                    <span class="price">
                                        <span class="sign">&#163;</span>
                                        <span class="currency">29</span>
                                        <span class="cent">.99</span>
                                        <span class="month">/MON</span>
                                    </span>
                                </div>
                            </div>
                            <div class="generic_feature_list">
                                <ul>
                                    <li><span>Full Access to Facilities</span></li>
                                    <li><span>Anytime Access to ALL Facilities</span></li>
                                    <li><span>Unlimited Body Analysis</span></li>
                                    <li><span>Exclusive Discounts on Classes</span></li>
                                    <li><span>24/7 Support</span></li>
                                </ul>
                            </div>
                            <div class="generic_price_btn clearfix">
                                <a class="" href="/buy">Buy</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        
        </div>
        <!-- Content Area Ends -->

        <!-- Footer Area Starts -->
		<footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h6>About</h6>
                        <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <h6>Quick Links</h6>
                        <ul class="footer-links">
                            <li><a href="/about">About Us</a></li>
                            <li><a href="#">Trainers</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="single-widget-home">
                            <h6>Newsletter</h6>
                            <p class="mb-4">Enter your email to get a newsletter every month!</p>
                            <form action="#">
                                <input type="email" placeholder="Your email here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email here'" required>
                                <button type="submit" class="template-btn"><i class="fa fa-long-arrow-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </footer>
        <!-- Footer Area Ends -->
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/js/mixitup.min.js"></script>
        <script src="assets/js/jquery.slicknav.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/nav.js"></script>
	</body>
</html>