<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<title>TEAM30 GYM</title>
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="../assets/fonts/flat-icon/flaticon.css">
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="../assets/css/style.css">
		<link rel="stylesheet" href="../assets/css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/themify-icons.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/table.css" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;600;700&display=swap" rel="stylesheet">
    </head>
    <body>
        <!-- Nav Area Starts -->
        <header>
            <nav>
                <div id="navbar">
                    <div id="logo" class="reverse">
                        <div class="mobile-btn" style="font-size:30px;cursor:pointer; font-weight:bold;" onclick="openNav()">&#9776;</div>
                        <div class="logo">Team30 <span>GYM</span></div>
                    </div>
                    <div id="links">
                        <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                        <a href="/receipt">Send Receipts</a>
                        <% } %>
                        <a href="/">Home</a>
                        <a href="/about">About Us</a>
                        <% if(request.getSession().getAttribute("membership") != null){ %>
                        <a href="/displayall">Activities</a>
                        <% } else{ %>
                        <a href="/memberships">Memberships</a>
                        <% } %>
												
                        <a href="/facilities">Facilities</a>
                        <% if(request.getSession().getAttribute("user") != null){ %>
                        <a href="/userbookings">My Bookings</a>
                        <a href="/logout">Logout</a>
                        <% } else{ %>
                        <a href="/login">Login</a>
                        <% } %>
                    </div>
                </div>
            </nav>
            <!-- Mobile Menu -->
            <div id="mySidenav" class="sidenav">
                <a style="cursor:pointer;" class="closebtn" onclick="closeNav()">&times;</a>
                <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                <a href="/receipt">Send Receipts</a>
                <% } %>
                <a href="/">Home</a>
                <a href="/about">About Us</a>
                <% if(request.getSession().getAttribute("membership") != null){ %>
                <a href="/displayall">Activities</a>
                <% } else{ %>
                <a href="/memberships">Memberships</a>
                <% } %>
								
                <a href="/facilities">Facilities</a>
                <% if(request.getSession().getAttribute("user") != null){ %>
                <a href="/userbookings">My Bookings</a>
                <a href="/logout">Logout</a>
                <% } else{ %>
                <a href="/login">Login</a>
                <% } %>
            </div>
        </header>
        <!-- Nav Area Ends -->

        <!-- Content Area Starts -->
        <div id="content-area">
            <div id="header-area">
                <h1 id="page-header">Gymnasium Activities</h1>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                	<th scope="col">Activity</th>
                                    <th scope="col">Day</th>
                                    <th scope="col">Start Time</th>
                                    <th scope="col">Closing time</th>
                                    <th scope="col">Capacity</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Booking Availability</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${gymnasiumServices}" var="ser">
                                <tr>
                                    <td><c:out value="${ser.serviceName}" /></td>
                                    <td><c:out value="${ser.date}" /></td>
                                    <td><c:out value="${ser.startTime}" /></td>
                                    <td><c:out value="${ser.endTime}" /></td>
                                    <td><c:out value="${ser.capacity}" /></td>
                                    <td><c:out value="${ser.price}" /></td>
                                    <td><a href="#" data-toggle="modal" data-target="#gym${ser.id}" class="btn btn-primary">Book now!</a></td>
								</tr>

								<!-- Pop-up Booking Confirmation -->
                                <div class="modal fade" id="gym${ser.id}">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header text-center">
												<h1 class="modal-title">Booking Confirmation</h1>
											</div>
											<div class="modal-body">
												<p><strong>Facility:</strong> ${ser.facility}</p>
												<p><strong>Activity:</strong> ${ser.serviceName}</p>
												<p><strong>Date:</strong> ${ser.date}</p>
												<p><strong>Time:</strong> ${ser.startTime} - ${ser.endTime}</p>
												<p><strong>Price:</strong> &#163;${ser.price}</p>
											</div>
											<div class="modal-footer">
												<a href="/paymentBooking?id=${ser.id}" class="btn btn-success">Continue to Payment</a>
												<input class="btn btn-danger" data-dismiss="modal" value="Close">
											</div>
										</div>
									</div>
								</div>
                                </c:forEach>
                             </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>

        <!-- Content Area Ends -->

        <!-- Footer Area Starts -->
		<footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h6>About</h6>
                        <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <h6>Quick Links</h6>
                        <ul class="footer-links">
                            <li><a href="/about">About Us</a></li>
                            <li><a href="#">Trainers</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="single-widget-home">
                            <h6>Newsletter</h6>
                            <p class="mb-4">Enter your email to get a newsletter every month!</p>
                            <form action="#">
                                <input type="email" placeholder="Your email here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email here'" required>
                                <button type="submit" class="template-btn"><i class="fa fa-long-arrow-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </footer>
        <!-- Footer Area Ends -->
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/js/mixitup.min.js"></script>
        <script src="assets/js/jquery.slicknav.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/nav.js"></script>
        <script src="assets/js/login.js"></script>
	</body>
</html>
