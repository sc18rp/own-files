<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<title>TEAM30 GYM</title>
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="../assets/fonts/flat-icon/flaticon.css">
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="../assets/css/style.css">
		<link rel="stylesheet" href="../assets/css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/themify-icons.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/register.css" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;600;700&display=swap" rel="stylesheet">

        <script type = "text/javascript">
            function validate(){
                alert("the username has been used");
            }
        </script>
    </head>
    <body>
        <!-- Nav Area Starts -->
        <header>
            <nav>
                <div id="navbar">
                    <div id="logo" class="reverse">
                        <div class="mobile-btn" style="font-size:30px;cursor:pointer; font-weight:bold;" onclick="openNav()">&#9776;</div>
                        <div class="logo">Team30 <span>GYM</span></div>
                    </div>
                    <div id="links">
                        <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                        <a href="/receipt">Send Receipts</a>
                        <% } %>
                        <a href="/">Home</a>
                        <a href="/about">About Us</a>
                        <% if(request.getSession().getAttribute("membership") != null){ %>
                        <a href="/displayall">Activities</a>
                        <% } else{ %>
                        <a href="/memberships">Memberships</a>
                        <% } %>
												
                        <a href="/facilities">Facilities</a>
                        <% if(request.getSession().getAttribute("user") != null){ %>
                        <a href="/userbookings">My Bookings</a>
                        <a href="/logout">Logout</a>
                        <% } else{ %>
                        <a href="/login">Login</a>
                        <% } %>
                    </div>
                </div>
            </nav>
            <!-- Mobile Menu -->
            <div id="mySidenav" class="sidenav">
                <a style="cursor:pointer;" class="closebtn" onclick="closeNav()">&times;</a>
                <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                <a href="/receipt">Send Receipts</a>
                <% } %>
                <a href="/">Home</a>
                <a href="/about">About Us</a>
                <% if(request.getSession().getAttribute("membership") != null){ %>
                <a href="/displayall">Activities</a>
                <% } else{ %>
                <a href="/memberships">Memberships</a>
                <% } %>
								
                <a href="/facilities">Facilities</a>
                <% if(request.getSession().getAttribute("user") != null){ %>
                <a href="/userbookings">My Bookings</a>
                <a href="/logout">Logout</a>
                <% } else{ %>
                <a href="/login">Login</a>
                <% } %>
            </div>
        </header>
        <!-- Nav Area Ends -->

        <!-- Content Area Starts -->
        <div id="content-area">
            <div id="header-area">
                <h1 id="page-header">Registration</h1>
            </div>
            <div class="wrapper">
                <div class="sct brand"></div>
				<div class="sct login">
                    <form method="POST" action="/register">
                        <label for="firstname">First Name:</label>
                        <input id="firstname" type="email2" name="firstname" required>

                        <label for="lastname">Last Name:</label>
                        <input id="lastname" type="email2" name="lastname" required>

                        <label for="email">Email Address:</label>
                        <input id="email" type="email2" name="email" pattern="[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*" title="Must follow email format" required>
                        
                        <label for="password">Password:</label>
                        <input id="password" type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters" aria-required="true" required>
                        
                        <label for="confirmpassword">Confirm Password:</label>
                        <input id="confirmpassword" type="password" name="confirmpassword" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must be Same as Password" aria-required="true" required>
                        
                        <% if(request.getSession().getAttribute("checkEmail") != null){ %>
                            <input type="submit" name="send" onclick="return validate();" value="Register">
                        <% } else { %>
                            <input type="submit" name="send" value="Register">
                        <% } %>
                        </form>
				</div> <!--end register-->
		    </div> <!--end wrapper-->
        </div>

        <!-- Content Area Ends -->

        <!-- Footer Area Starts -->
		<footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h6>About</h6>
                        <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <h6>Quick Links</h6>
                        <ul class="footer-links">
                            <li><a href="/about">About Us</a></li>
                            <li><a href="#">Trainers</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="single-widget-home">
                            <h6>Newsletter</h6>
                            <p class="mb-4">Enter your email to get a newsletter every month!</p>
                            <form action="#">
                                <input type="email" placeholder="Your email here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email here'" required>
                                <button type="submit" class="template-btn"><i class="fa fa-long-arrow-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </footer>
        <!-- Footer Area Ends -->
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/js/mixitup.min.js"></script>
        <script src="assets/js/jquery.slicknav.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/nav.js"></script>
        <script src="assets/js/login.js"></script>
	</body>
</html>