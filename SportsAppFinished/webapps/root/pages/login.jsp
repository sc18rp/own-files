<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<title>TEAM30 GYM</title>
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="../assets/fonts/flat-icon/flaticon.css">
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="../assets/css/style.css">
		<link rel="stylesheet" href="../assets/css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/themify-icons.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/login.css" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;600;700&display=swap" rel="stylesheet">

        <script type = "text/javascript">
            function validate(){
                alert("invalid username or password");
                window.location.assign("http://localhost:8080/login");
            }
        </script>
    </head>
    <body>
        <!-- Nav Area Starts -->
        <header>
            <nav>
                <div id="navbar">
                    <div id="logo" class="reverse">
                        <div class="mobile-btn" style="font-size:30px;cursor:pointer; font-weight:bold;" onclick="openNav()">&#9776;</div>
                        <div class="logo">Team30 <span>GYM</span></div>
                    </div>
                    <div id="links">
                        <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                        <a href="/receipt">Send Receipts</a>
                        <% } %>
                        <a href="/">Home</a>
                        <a href="/about">About Us</a>
                        <% if(request.getSession().getAttribute("membership") != null){ %>
                        <a href="/displayall">Activities</a>
                        <% } else{ %>
                        <a href="/memberships">Memberships</a>
                        <% } %>
												
                        <a href="/facilities">Facilities</a>
                        <% if(request.getSession().getAttribute("user") != null){ %>
                        <a href="/userbookings">My Bookings</a>
                        <a href="/logout">Logout</a>
                        <% } else{ %>
                        <a href="/login">Login</a>
                        <% } %>
                    </div>
                </div>
            </nav>
            <!-- Mobile Menu -->
            <div id="mySidenav" class="sidenav">
                <a style="cursor:pointer;" class="closebtn" onclick="closeNav()">&times;</a>
                <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                <a href="/receipt">Send Receipts</a>
                <% } %>
                <a href="/">Home</a>
                <a href="/about">About Us</a>
                <% if(request.getSession().getAttribute("membership") != null){ %>
                <a href="/displayall">Activities</a>
                <% } else{ %>
                <a href="/memberships">Memberships</a>
                <% } %>
								
                <a href="/facilities">Facilities</a>
                <% if(request.getSession().getAttribute("user") != null){ %>
                <a href="/userbookings">My Bookings</a>
                <a href="/logout">Logout</a>
                <% } else{ %>
                <a href="/login">Login</a>
                <% } %>
            </div>
        </header>
        <!-- Nav Area Ends -->

        <!-- Content Area Starts -->
        <div id="content-area">
            <div id="header-area">
                <h1 id="page-header">Member Login</h1>
            </div>

            <div class="wrapper">
                <div class="sct brand"></div>
				<div class="sct login">
                    <% if(request.getSession().getAttribute("user") != null){ %>
                        <h3>You are already logged in as:  <%= request.getSession().getAttribute("user") %></h3>
                    <% } else{ %>
                        <form method="POST" action="/login">
                            </br>
                            <tr>
                                <td>Username:</td>
                                <td><input size="12" value="" name="username" maxlength="25" type="text" required></td>
                            </tr>
                            <tr>
                                <td>Password:</td>
                                <td><input size="12" value="" name="password" maxlength="25" type="password" aria-required="true" required></td>
                            </tr>
                            <div class="forgot-remember">
                                <label class="control control-checkbox">
                                    Remember me
                                    <input type="checkbox" />
                                    <div class="control_indicator"></div>
                                </label>
                                <div class="forgot">
                                    <a href="/register">New user</a>
                                </div>
                            </div>
                            <tr>
                                <td colspan="2" align="center">
                                    <% if(request.getSession().getAttribute("match") != null){ %>
                                        <input type="submit" onclick="return validate()" value="Submit">
                                    <% } else { %>
                                        <input type="submit" value="Submit">
                                    <% } %>
                                </td>
                            </tr>
                        </form>
                    <% } %>
				</div> <!--end login-->
		    </div> <!--end wrapper-->
        </div>

        <!-- Content Area Ends -->

        <!-- Footer Area Starts -->
		<footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h6>About</h6>
                        <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <h6>Quick Links</h6>
                        <ul class="footer-links">
                            <li><a href="/about">About Us</a></li>
                            <li><a href="#">Trainers</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="single-widget-home">
                            <h6>Newsletter</h6>
                            <p class="mb-4">Enter your email to get a newsletter every month!</p>
                            <form action="#">
                                <input type="email" placeholder="Your email here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email here'" required>
                                <button type="submit" class="template-btn"><i class="fa fa-long-arrow-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </footer>
        <!-- Footer Area Ends -->
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/js/mixitup.min.js"></script>
        <script src="assets/js/jquery.slicknav.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/nav.js"></script>
        <script src="assets/js/login.js"></script>
	</body>
</html>