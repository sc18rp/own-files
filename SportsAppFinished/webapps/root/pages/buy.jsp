<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<title>TEAM30 GYM</title>
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="../assets/fonts/flat-icon/flaticon.css">
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="../assets/css/style.css">
		<link rel="stylesheet" href="../assets/css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/slicknav.min.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/themify-icons.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/login.css" type="text/css">


	</head>
	<body>

		<!-- Header Area Starts -->
		<header>
            <nav>
                <div id="navbar">
                    <div id="logo" class="reverse">
                        <div class="mobile-btn" style="font-size:30px;cursor:pointer; font-weight:bold;" onclick="openNav()">&#9776;</div>
                        <div class="logo">Team30 <span>GYM</span></div>
                    </div>
                    <div id="links">
                        <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                        <a href="/receipt">Send Receipts</a>
                        <% } %>
                        <a href="/">Home</a>
                        <a href="/about">About Us</a>
                        <% if(request.getSession().getAttribute("membership") != null){ %>
                        <a href="/displayall">Activities</a>
                        <% } else{ %>
                        <a href="/memberships">Memberships</a>
                        <% } %>
												
                        <a href="/facilities">Facilities</a>
                        <% if(request.getSession().getAttribute("user") != null){ %>
                        <a href="/userbookings">My Bookings</a>
                        <a href="/logout">Logout</a>
                        <% } else{ %>
                        <a href="/login">Login</a>
                        <% } %>
                    </div>
                </div>
            </nav>
            <!-- Mobile Menu -->
            <div id="mySidenav" class="sidenav">
                <a style="cursor:pointer;" class="closebtn" onclick="closeNav()">&times;</a>
                <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                <a href="/receipt">Send Receipts</a>
                <% } %>
                <a href="/">Home</a>
                <a href="/about">About Us</a>
                <% if(request.getSession().getAttribute("membership") != null){ %>
                <a href="/displayall">Activities</a>
                <% } else{ %>
                <a href="/memberships">Memberships</a>
                <% } %>
								
                <a href="/facilities">Facilities</a>
                <% if(request.getSession().getAttribute("user") != null){ %>
                <a href="/userbookings">My Bookings</a>
                <a href="/logout">Logout</a>
                <% } else{ %>
                <a href="/login">Login</a>
                <% } %>
            </div>
        </header>
		<!-- Header Area End -->


		<!-- Body Area Start -->
		<div class="wrapper">
        <div class="sct brand"></div>
        <div class="sct login">



					<% if(request.getAttribute("paymentType") == null){ %>

						<form method="POST" action="/buy">
                <h3>Buy a Membership</h3>
              </br>
								<p>Membership Type</p>
								<div class="month">
									<select name="membershipType">
										<option value="basic">Basic</option>
										<option value="premium">Premium</option>
										<option value="platinum">Platinum</option>
									</select>
								</div>
								<p>Membership Length</p>
								<div class="month">
									<select name="membershipLength">
										<option value="annual">Annual</option>
										<option value="monthly">Monthly</option>
									</select>
								</div>
								<p>Payment type</p>
								<div class="month">
									<select name="paymentType">
										<option value="cash">Cash</option>
										<option value="card">Card</option>
									</select>
								</div>
								<input type="submit" name="send" value="Proceed">

            </form>
					<% } else if(request.getAttribute("paymentType").equals("cash")){%>
						<h2>Your request has been stored!</h2></br>
						<p1>You can pay cash in our reception</p1>

					<% } else{ %>
						<h2>Your are now our member!</h2></br>
						<p1>We are happy you chose us!</p1>
						<a href="/view">View receipt</a>
					<% } %>
        </div> <!--end login-->
    </div> <!--end wrapper-->
		<!-- Body Area End -->


		<!-- Footer Area Starts -->
		<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>About</h6>
            <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul class="footer-links">
              <li><a href="http://scanfcode.com/about/">About Us</a></li>
              <li><a href="http://scanfcode.com/contribute-at-scanfcode/">Trainers</a></li>
              <li><a href="http://scanfcode.com/privacy-policy/">Privacy Policy</a></li>
              <li><a href="http://scanfcode.com/sitemap/">Contact</a></li>
            </ul>
          </div>
					<div class="col-xs-6 col-md-3">
								<div class="single-widget-home">
										<h6>Newsletter</h6>
										<p class="mb-4">Enter your email to get a newsletter every month!</p>
										<form action="#">
												<input type="email" placeholder="Your email here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email here'" required>
												<button type="submit" class="template-btn"><i class="fa fa-long-arrow-right"></i></button>
										</form>
								</div>
					</div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a class="linkedin" href="#"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
</footer>
<!-- Footer Area Ends -->
<script src="assets/js/login.js"></script>
	</body>
</html>
