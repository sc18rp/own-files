<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<title>TEAM30 GYM</title>
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="../assets/fonts/flat-icon/flaticon.css">
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="../assets/css/style.css">
		<link rel="stylesheet" href="../assets/css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/slicknav.min.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/themify-icons.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/facilities.css" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;600;700&display=swap" rel="stylesheet">
    </head>

    <body>
		<!-- Header Area Starts -->
    <header>
      <nav>
          <div id="navbar">
              <div id="logo" class="reverse">
                  <div class="mobile-btn" style="font-size:30px;cursor:pointer; font-weight:bold;" onclick="openNav()">&#9776;</div>
                  <div class="logo">Team30 <span>GYM</span></div>
              </div>
              <div id="links">
                  <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
                  <a href="/receipt">Send Receipts</a>
                  <% } %>
                  <a href="/">Home</a>
                  <a href="/about">About Us</a>
                  <% if(request.getSession().getAttribute("membership") != null){ %>
                  <a href="/displayall">Activities</a>
                  <% } else{ %>
                  <a href="/memberships">Memberships</a>
                  <% } %>
                  
                  <a href="/facilities">Facilities</a>
                  <% if(request.getSession().getAttribute("user") != null){ %>
                  <a href="/userbookings">My Bookings</a>
                  <a href="/logout">Logout</a>
                  <% } else{ %>
                  <a href="/login">Login</a>
                  <% } %>
              </div>
          </div>
      </nav>
      <!-- Mobile Menu -->
      <div id="mySidenav" class="sidenav">
          <a style="cursor:pointer;" class="closebtn" onclick="closeNav()">&times;</a>
          <% if(request.getSession().getAttribute("role")!= null && request.getSession().getAttribute("role").equals("employee")){ %>
          <a href="/receipt">Send Receipts</a>
          <% } %>
          <a href="/">Home</a>
          <a href="/about">About Us</a>
          <% if(request.getSession().getAttribute("membership") != null){ %>
          <a href="/displayall">Activities</a>
          <% } else{ %>
          <a href="/memberships">Memberships</a>
          <% } %>
          
          <a href="/facilities">Facilities</a>
          <% if(request.getSession().getAttribute("user") != null){ %>
          <a href="/userbookings">My Bookings</a>
          <a href="/logout">Logout</a>
          <% } else{ %>
          <a href="/login">Login</a>
          <% } %>
      </div>
  </header>
		<!-- Header Area End -->


        <!-- Content Area Start -->
        <div id="content-area">
            <div id="header-area">
                <h1 id="page-header">Our Facilities</h1>
            </div>

            <!-- Carousel Start -->
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                  <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                  <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="../assets/images/banner-pool2.jpeg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Swimming Pool</h5>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img src="../assets/images/banner-gym2.jpeg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Gymnasium</h5>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img src="../assets/images/banner-squash2.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Squash Courts</h5>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img src="../assets/images/banner-hall3.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>Multipurpose Sports Hall</h5>
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
              <!-- Carousel End -->

            <div class="container">
                <div class="row">
                    <div class="col-md-6 mb-4">
                        <div class="card h-100 mb-4">
                            <img src="../assets/images/icon-pool2.jpeg" class="card-img-top" alt="swimming pool">
                            <div class="card-body d-flex flex-column">
                              <h5 class="card-title"><strong>Swimming Pool</strong></h5>
                              <hr>
                              <p class="card-text">
                                <p>Join classes or unwind in our 6 lanes 25metres indoor heated pool. Dive towers of 1,3,5 metres are also available.</p>
                              </p>
                              <a href="display" class="btn btn-primary mt-auto">More Info</a>
                            </div>
                          </div>
                    </div>

                    <div class="col-md-6 mb-4">
                        <div class="card h-100">
                            <img src="../assets/images/icon-gym2.jpeg" class="card-img-top" alt="gymnasium">
                            <div class="card-body d-flex flex-column">
                              <h5 class="card-title"><strong>Gymnasium</strong></h5>
                              <hr>
                              <p class="card-text">
                                <p>Our state of the art gymnasium comes fully equipped with modern cardiovascular and resistance machines, an extensive
                                    free-weights area, and various fitness specialist equipments to cater to any and everyone.</p>
                              </p>
                              <a href="displaygymn" class="btn btn-primary mt-auto">More Info</a>
                            </div>
                          </div>
                    </div>

                    <div class="col-md-6 mb-4">
                        <div class="card h-100">
                            <img src="../assets/images/icon-squash2.jpeg" class="card-img-top" alt="squash courts">
                            <div class="card-body d-flex flex-column">
                              <h5 class="card-title"><strong>Squash Courts</strong></h5>
                              <hr>
                              <p class="card-text">
                                <p>4 squash courts installed with hardwood floors, quality walls, and viewing balconies to provide you with the most
                                    optimal squash experience.</p>
                              </p>
                              <a href="displaysquash" class="btn btn-primary mt-auto">More Info</a>
                            </div>
                          </div>
                    </div>

                    <div class="col-md-6 mb-4">
                        <div class="card h-100">
                            <img src="../assets/images/icon-hall2.jpeg" class="card-img-top" alt="Sports Hall">
                            <div class="card-body d-flex flex-column">
                              <h5 class="card-title"><strong>Sports Hall</strong></h5>
                              <hr>
                              <p class="card-text">
                                <p>Large multipurpose sports hall which seats up to 500 people. Catered to various sports such as Basketball,
                                    Badminton, Netball, and any other indoor sports.</p>
                              </p>
                              <a href="displaymulti" class="btn btn-primary mt-auto">More Info</a>
                            </div>
                          </div>
                    </div>

                </div>
            </div>


        </div>


		<!-- Footer Area Starts -->
		<footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h6>About</h6>
                        <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <h6>Quick Links</h6>
                        <ul class="footer-links">
                            <li><a href="/about">About Us</a></li>
                            <li><a href="#">Trainers</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="single-widget-home">
                            <h6>Newsletter</h6>
                            <p class="mb-4">Enter your email to get a newsletter every month!</p>
                            <form action="#">
                                <input type="email" placeholder="Your email here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email here'" required>
                                <button type="submit" class="template-btn"><i class="fa fa-long-arrow-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </footer>
        <!-- Footer Area Ends -->
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/js/mixitup.min.js"></script>
        <script src="assets/js/jquery.slicknav.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/nav.js"></script>
	</body>
</html>
