<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
	<head>
		<title>My Web App</title>
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="../assets/fonts/flat-icon/flaticon.css">
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="../assets/css/style.css">
      <link rel="stylesheet" href="../assets/css/table.css">
		<link rel="stylesheet" href="../assets/css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/slicknav.min.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/themify-icons.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/login.css" type="text/css">
		<link rel="stylesheet" href="../assets/css/fac.css" type="text/css">

	</head>
	<body>

		<!-- Header Area Starts -->
		<header>
		    <nav>
		      <div id="navbar">
		        <div id="logo" class="reverse">
		          <div class="mobile-btn" style="font-size:30px;cursor:pointer; font-weight:bold;" onclick="openNav()">&#9776;</div>
		          <div class="logo">Team30 <span>GYM ADMIN</span></div>

		        </div>
		        <div id="links">
              <a href="/admin">Facilities</a>
              <a href="/fac">New Facility</a>
              <a href="/employ">Register Employee</a>
              <a href="/logout">Logout</a>
		        </div>
		      </div>

		    </nav>
		    <!-- Mobile Menu -->
		    <div id="mySidenav" class="sidenav">
					<a style="cursor:pointer;" class="closebtn" onclick="closeNav()">&times;</a>
          <a href="/admin">Facilities</a>
          <a href="/fac">New Facility</a>
          <a href="/employ">Register Employee</a>
          <a href="/logout">Logout</a>
			</div>
		  </header>
		  <!--Header Area Ends -->


		  <!--Content Area Starts-->
      	<div>
        <% if(request.getParameter("err")!= null && request.getParameter("err").equals("true")){ %>
          <h2>This facility already exists</h2>
        <% } else if(request.getParameter("err")!= null && !request.getParameter("err").equals("true")){ %>
          <h2>New facility was created</h2>
		<% } %>
		
		<div id= "content-area">
			<div id ="header-area">
				<h1 id= "page-header">New Facility</h1>
			</div>

		</div>
		<div class = "fac-Pad">
				<form action="/fac" method="POST">
					<p>Name:</p>
					<input type="text" name="name"  required>
					<br/><br/>
					<p>Amount:</p>
					<input type="number" name="capacity" required>
					<br/><br/>
					<input type="submit" value="Submit">
				  </form>
		</div>
	  </div>
		  <!--Content Area Ends-->
		  

	      <!-- Footer Area Starts -->
		<footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h6>About</h6>
                        <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <h6>Quick Links</h6>
                        <ul class="footer-links">
							<li><a href="/about">About Us</a></li>
							<li><a href="#">Trainers</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="single-widget-home">
                            <h6>Newsletter</h6>
                            <p class="mb-4">Enter your email to get a newsletter every month!</p>
                            <form action="#">
                                <input type="email" placeholder="Your email here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email here'" required>
                                <button type="submit" class="template-btn"><i class="fa fa-long-arrow-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </footer>
        <!-- Footer Area Ends -->
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/js/mixitup.min.js"></script>
        <script src="assets/js/jquery.slicknav.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/main.js"></script>
		<script src="assets/js/nav.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

	</body>
</html>
