// javac -cp /home/csunix/sc18ir/Desktop/jetty/team30/lib/servlet-api-3.1.jar LoginServlet.java

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {


		DatabaseManager database = new DatabaseManager();

		if(request.getSession().getAttribute("user") == null){
			request.getRequestDispatcher("pages/login.jsp").forward(request,response);
		}
		else{
			String username = request.getSession().getAttribute("user").toString();
			if(database.getRole(username) != null && "admin".equals(database.getRole(username))){
	      response.sendRedirect("/admin");
	    }
	    else
				response.sendRedirect("pages/login.jsp");
		}
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		DatabaseManager database = new DatabaseManager();

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String match = "";

		// check if details are correct with database
		if(database.match(username,password)){

			request.getSession().setAttribute("match", match);
			request.getSession().setAttribute("user", username);
			request.getSession().setAttribute("role", database.getRole(username));
			if(database.getMembershipData(username).size() > 0){
				String type = database.getMembershipData(username).get(0).getType();
				request.getSession().setAttribute("membership", type);
			}
		}
		if(database.getRole(username) != null && "admin".equals(database.getRole(username))){
      response.sendRedirect("/admin");
	}
		else if(!database.match(username,password)){
			match = "notMatched";
			request.getSession().setAttribute("match",match);
			response.sendRedirect("/");

		}
		else{
		response.sendRedirect("/");
		}
	}
}
