import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AdminServlet extends HttpServlet {

  private List<Facility> facilities = null;

  @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    DatabaseManager db = new DatabaseManager();

		String name = request.getParameter("sss");


    if(name != null)
      db.unregisterFacility(name);

    facilities = db.getAllFacilities();

    request.setAttribute("facilities",facilities);

    String username = request.getSession().getAttribute("user").toString();

    if(db.getRole(username) != null && "admin".equals(db.getRole(username))){
      request.getRequestDispatcher("adminpages/facilities.jsp").forward(request,response);
    }
    else{
      response.sendRedirect("/");
    }
	}
}
