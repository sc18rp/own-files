import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class NewFacilityServlet extends HttpServlet {


  @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    DatabaseManager db = new DatabaseManager();
    String username = request.getSession().getAttribute("user").toString();


    if(db.getRole(username) != null && "admin".equals(db.getRole(username))){
      request.getRequestDispatcher("adminpages/fac.jsp").forward(request,response);
    }
    else{
      response.sendRedirect("/");
    }
	}

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    String name = request.getParameter("name");
    int amount = Integer.parseInt(request.getParameter("capacity"));

    DatabaseManager db = new DatabaseManager();

    try{
      db.registerFacility(name,amount);
      response.sendRedirect("/fac?err=false");
    }
    catch (IllegalArgumentException e) {
      response.sendRedirect("/fac?err=true");
    }

    response.sendRedirect("/fac");
  }
}
