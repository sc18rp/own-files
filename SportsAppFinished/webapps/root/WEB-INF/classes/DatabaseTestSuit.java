import java.util.*;
import java.io.*;
import java.text.*;
import java.util.Date;

import org.sql2o.*;

import org.junit.*;
import static org.junit.Assert.*;

public class DatabaseTestSuit {

  private static Sql2o dummy;
  private DatabaseManager testable;
  private final static String[] tables = {"accounts", "facilities", "services", "members","bookings"};

  private static int clearAllTables() {

    int cleared = 0;
    for (String table : tables) {
      try (Connection t = dummy.open()) {

          String q = "DELETE FROM " + table;
          Query query = t.createQuery(q);
          query.executeUpdate();
          cleared++;
      } catch (Exception ex) {
        System.err.println("Can't clear table : " + table);
      }
    }

    if(cleared!=tables.length) {
      System.out.println("WARNING : List of tables may be inaccurate.");

    }
    return cleared;

  }

  @BeforeClass
  public static void initDummyDB() {

    dummy = new Sql2o("jdbc:sqlite:dummy.db","","");
    System.out.println("TestSuit : Connected to dummy.db;");

    System.out.println("NOTE : TestSuit is using table list provided as a constant in DatabaseTestSuit.java;");
    System.out.println("to update table list please update the constant.");

  }

  @Before
  public void reset() {

    //reset DatabaseManager state
    testable = new DatabaseManager("dummy.db");

    //clear all tables
    clearAllTables();

  }

  @AfterClass
  public static void wrapUp() {
    clearAllTables();
  }

  @Test
  public void test_registerFacility() {

    testable.registerFacility("test",1);

    List<Facility> all;
    try (Connection t = dummy.open()) {

      String q = "SELECT * FROM facilities WHERE name = :t";
      Query query = t.createQuery(q).addParameter("t","test");
      all = query.executeAndFetch(Facility.class);

    }

    assertEquals(all.size(),1);
    assertEquals("test",all.get(0).getName());

  }

  @Test
  public void test_unregisterFacility() {

    testable.registerFacility("test",1);
    testable.unregisterFacility("test");

    List<Facility> all;
    try (Connection t = dummy.open()) {

      String q = "SELECT * FROM facilities";
      Query query = t.createQuery(q);
      all = query.executeAndFetch(Facility.class);

    }

    assertEquals(0,all.size());

    boolean refused = false;
    try {
      testable.unregisterFacility("test");
    } catch (IllegalArgumentException ex) {
      refused = true;
    }

    assertEquals(true,refused);

  }

  @Test
  public void test_addServiceTimeSlot() {

    testable.registerFacility("F",5);
    testable.addServiceTimeSlot("name","day","start","end","F",1,0);

    List<Service> s;
    try (Connection t = dummy.open()) {

      String q = "SELECT * FROM services";
      Query query = t.createQuery(q);
      s = query.executeAndFetch(Service.class);

    }

    assertEquals(1,s.size());
    assertEquals(s.get(0).getName(),"name");

  }


  @Test
  public void test_removeServiceTimeSlot() {

    testable.registerFacility("F",5);
    testable.addServiceTimeSlot("name","day","start","end","F",1,0);
    testable.removeServiceTimeSlot("name","day","start","end","F");

    List<Service> s;
    try (Connection t = dummy.open()) {

      String q = "SELECT * FROM services";
      Query query = t.createQuery(q);
      s = query.executeAndFetch(Service.class);

    }

    assertEquals(0,s.size());

  }


  @Test
  public void test_serviceIDAssignment() {

    testable.registerFacility("F",5);
    testable.addServiceTimeSlot("0","day","stat","endd","F",1,0); //has id=0
    testable.addServiceTimeSlot("1","day","stat","endd","F",1,0); //has id=1
    testable.removeServiceTimeSlot("0","day","stat","endd","F"); //id=0 freed up
    testable.addServiceTimeSlot("2","day","stat","endd","F",1,0); //has id=0

    List<Service> s;
    try (Connection t = dummy.open()) {

      String q = "SELECT * FROM services";
      Query query = t.createQuery(q);
      s = query.executeAndFetch(Service.class);

    }

    assertEquals(1,s.get(0).getID());
    assertEquals(0,s.get(1).getID());

  }


  @Test
  public void test_bookService() {

    testable.registerFacility("F",5);
    testable.register("email","fname","lname","pass","admin");
    testable.addServiceTimeSlot("0","day","stat","endd","F",1,0);
    List<Service> allS = testable.getAllServices();

    testable.bookService("email",allS.get(0));

    List<Booking> b;
    try (Connection t = dummy.open()) {

      String q = "SELECT * FROM bookings";
      Query query = t.createQuery(q);
      b = query.executeAndFetch(Booking.class);

    }

    assertEquals(1,b.size());
    assertEquals(allS.get(0).getID(),b.get(0).getServiceID());

  }

  @Test
  public void test_cancelBookingOfService() {

    testable.registerFacility("F",5);
    testable.register("email","fname","lname","pass","admin");
    testable.addServiceTimeSlot("0","day","stat","endd","F",1,0);
    testable.addServiceTimeSlot("1","day","stat","endd","F",1,0);

    List<Service> allS = testable.getAllServices();
    testable.bookService("email",allS.get(0));
    testable.bookService("email",allS.get(1));

    testable.cancelBookingOfService("email",allS.get(0));

    List<Booking> b;
    try (Connection t = dummy.open()) {

      String q = "SELECT * FROM bookings";
      Query query = t.createQuery(q);
      b = query.executeAndFetch(Booking.class);

    }

    assertEquals(1,b.size());
    assertEquals(allS.get(1).getID(),b.get(0).getServiceID());
  }


  @Test
  public void test_registerMembership() {

    testable.register("email","fname","lname","pass","admin");
    testable.registerMembership("email","supremo","28.09.2000");

    List<Membership> m;
    try (Connection t = dummy.open()) {

      String q = "SELECT * FROM members";
      Query query = t.createQuery(q);
      m = query.executeAndFetch(Membership.class);

    }

    assertEquals(1,m.size());
    assertEquals("email",m.get(0).getEmail());
    assertEquals("supremo",m.get(0).getType());
    assertEquals("28.09.2000",m.get(0).getExpiryDate());

    Date membershipExpiryDate;
    Date inputDate;
    try {
      membershipExpiryDate = m.get(0).getDateAsDateObj();
      inputDate = new SimpleDateFormat("dd.mm.yyyy").parse("28.09.2000");
    } catch (ParseException pex) {
      assertTrue(false);
      return;
    }

    assertEquals(inputDate,membershipExpiryDate);

    try {
      testable.registerMembership("unregistered_user@shadywebsite.com","supremo","28.09.2000");
    } catch (IllegalArgumentException iex) {
      assertTrue(true);
      return;
    }

    assertTrue(false);
  }


  @Test
  public void test_getMembershipData() {

    testable.register("email","fname","lname","pass","admin");
    testable.registerMembership("email","supremo","28.09.2000");

    List<Membership> m = testable.getMembershipData("email");

    assertEquals(1,m.size());
    assertEquals("email",m.get(0).getEmail());
    assertEquals("supremo", m.get(0).getType());
    assertEquals("28.09.2000", m.get(0).getExpiryDate());
  }

  @Test
  public void test_removeMembership() {

    testable.register("email","fname","lname","pass","admin");
    testable.register("email2","fname","lname","pass","admin");
    testable.registerMembership("email","supremo","28.09.2000");
    testable.registerMembership("email2","supremo+","28.09.2000");

    testable.removeMembership("email");

    List<Membership> m;
    try (Connection t = dummy.open()) {

      String q = "SELECT * FROM members";
      Query query = t.createQuery(q);
      m = query.executeAndFetch(Membership.class);

    }

    assertEquals(1, m.size());
    assertEquals("email2",m.get(0).getEmail());

  }
  //Tests for Account and bank
  @Test
  public void test_register(){
    try{
      testable.register("verynewemailtest","fn","ln","ps","ad");

      List<Account> all;
      try (Connection t = dummy.open()) {

        String q = "SELECT * FROM accounts WHERE email = :t";
        Query query = t.createQuery(q).addParameter("t","verynewemailtest");
        all = query.executeAndFetch(Account.class);
      }

      assertEquals(all.size(),1);
      assertEquals("verynewemailtest",all.get(0).getEmail());

      testable.delete("verynewemailtest");
    }
    catch(Exception e){
      System.out.println("test_delete function does not work");
      assertTrue(true);
    }
  }

  @Test
  public void test_delete(){
  try{
    testable.register("verynewemailtest","fn","ln","ps","ad");
    assertEquals(testable.userExists("verynewemailtest"),true);

    testable.delete("verynewemailtest");
    assertEquals(testable.userExists("verynewemailtest"),false);
  }
  catch(Exception e){
      System.out.println("test_delete function does not work");
      assertTrue(true);
    }
  }

  @Test
  public void test_changeRole_getRole(){
    try{
      testable.register("verynewemailtest","fn","ln","ps","theRole");
      assertEquals(testable.getRole("verynewemailtest"),"theRole");
      testable.changeRole("verynewemailtest", "newRole");
      assertEquals(testable.getRole("verynewemailtest"),"newRole");

      testable.delete("verynewemailtest");
    }
    catch(Exception e){
      System.out.println("test_changeRole_getRole function does not work");
      assertTrue(true);
    }

  }

  @Test
  public void test_match(){
    try{
      testable.register("verynewemailtest","fn","ln","ps","rl");
      assertEquals(testable.match("verynewemailtest", "ps"), true);

      testable.delete("verynewemailtest");
      assertEquals(testable.match("verynewemailtest", "ps"), false);
    }
    catch(Exception e){
      System.out.println("test_match function does not work");
      assertTrue(true);
    }
  }

  @Test
  public void test_add_deleteCard(){

    try{
      testable.register("UserName","fn","ln","ps","ad");
      testable.addCard("UserName", "1234567890", "SomeCardName", 123456, 553);

      List<BankAccount> all;
      try (Connection t = dummy.open()) {

        String q = "SELECT * FROM bankAccount WHERE email = :t";
        Query query = t.createQuery(q).addParameter("t","UserName");
        all = query.executeAndFetch(BankAccount.class);
      }
      //To check later (after deleting the card)

      assertEquals(1234567890, all.get(0).getCardNumber());
      assertEquals("SomeCardName", all.get(0).getCardName());

      testable.deleteCard("UserName", "1234567890");
      try (Connection t = dummy.open()) {

        String q = "SELECT * FROM bankAccount WHERE email = :t";
        Query query = t.createQuery(q).addParameter("t","UserName");
        all = query.executeAndFetch(BankAccount.class);
      }

      String stringName = null;
      int stringNumber = 0;
      if(!all.isEmpty()){
        stringName = all.get(0).getCardName();
        stringNumber = all.get(0).getCardNumber();
      }

      // Does not exist
      assertEquals(stringName, null);
      assertEquals(stringNumber, 0);

      testable.delete("UserName");
    }
    catch(Exception e){
      System.out.println("test_add_deleteCard function does not work");
      assertTrue(true);
    }
  }

}

