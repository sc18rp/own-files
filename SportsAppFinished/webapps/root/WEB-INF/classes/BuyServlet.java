import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BuyServlet extends HttpServlet {

  private String type = null;
  private String length = null;
  private String payment = null;

  @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    if( request.getSession().getAttribute("user") != null){
      if(request.getSession().getAttribute("membership") == null){


        request.setAttribute("paymentType",payment);

        if(payment != null && payment.equals("cash")){
          request.getSession().setAttribute("membership", "requested");
        }
        else if(payment != null && payment.equals("card")){
          request.getSession().setAttribute("membership", type);
        }

        request.getRequestDispatcher("pages/buy.jsp").forward(request,response);
        payment = null;
      }
      else{
        request.getRequestDispatcher("/").forward(request,response);
      }
    }
    else{
      request.getRequestDispatcher("/login").forward(request,response);
    }

	}

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    type = request.getParameter("membershipType");
    length = request.getParameter("membershipLength");
    payment = request.getParameter("paymentType");

    if(payment.equals("card")){
      DatabaseManager db = new DatabaseManager();
      db.registerMembership(request.getSession().getAttribute("user").toString(), type, "20.02.2020");
      request.getSession().setAttribute("membership", type);
    }
    else{
      // TODO: Link with receipt database!!!!!!!!!!!!!
    }

    response.sendRedirect("/buy");
  }
}
