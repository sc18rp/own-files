import java.text.*;
import java.util.Date;

public class Membership {

  String email;
  String type;
  String expiryDate;

  public String getEmail() {
    return email;
  }

  public String getType() {
    return type;
  }

  public String getExpiryDate() {
    return expiryDate;
  }

  public String toString() {
    return email + " is a " + type + " member until " + expiryDate;
  }

  public Date getDateAsDateObj() throws ParseException {
    Date exDate = new SimpleDateFormat("dd.mm.yyyy").parse(expiryDate);
    return exDate;
  }

  public boolean isExpired() throws ParseException {
    Date dateNow = new Date();
    if (dateNow.compareTo(getDateAsDateObj())>0) return true;
    else return false;
  }

  public static boolean isValidDateFormat(String date) {

    try {
      Date exDate = new SimpleDateFormat("dd.mm.yyyy").parse(date);
    } catch (ParseException pex) {
      return false;
    }
    return true;

  }

}
