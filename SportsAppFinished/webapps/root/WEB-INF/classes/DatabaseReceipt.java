import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;

import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import org.mindrot.jbcrypt.BCrypt;

public class DatabaseReceipt {

  private Sql2o db;

  //default constructor to be used
  public DatabaseReceipt() {
      //establish connection with our default db
      db = new Sql2o("jdbc:sqlite:accounts.db","","");
  }

  public List<Integer> getDates(String email){

    List<Receipts> receipts;

    try(Connection t = db.open()) {

      String getQuery = "SELECT date FROM receipts WHERE email = :e";
      Query query = t.createQuery(getQuery).addParameter("e", email);
      receipts = query.executeAndFetch(Receipts.class);


    } catch (Exception ex) {
        System.err.println("Can't get the date : " + ex.getMessage());
        return null;
    }

    if (receipts == null) return null;
    else {
        List<Integer> dates = new LinkedList<>();
        for (int i = 0; i < receipts.size(); i++) {
            dates.add(receipts.get(i).getDate());
        }

        return dates;
    }
  }

  public List<Double> getPayedSums(String email){

    List<Receipts> receipts;

    try(Connection t = db.open()) {

      String getQuery = "SELECT payed FROM receipts WHERE email = :e";
      Query query = t.createQuery(getQuery).addParameter("e", email);
      receipts = query.executeAndFetch(Receipts.class);

    } catch (Exception ex) {
        System.err.println("Can't get sum : " + ex.getMessage());
        return null;
    }

    if (receipts == null) return null;
    else {
        List<Double> payedSums = new LinkedList<>();
        for (int i = 0; i < receipts.size(); i++) {
            payedSums.add(receipts.get(i).getPayed());
        }

        return payedSums;
    }
  }

  public List<String> getPaymentMethods(String email){

    List<Receipts> receipts;

    try(Connection t = db.open()) {

      String getQuery = "SELECT payment FROM receipts WHERE email = :e";
      Query query = t.createQuery(getQuery).addParameter("e", email);
      receipts = query.executeAndFetch(Receipts.class);

    } catch (Exception ex) {
        System.err.println("Can't get sum : " + ex.getMessage());
        return null;
    }

    if (receipts == null) return null;
    else {
        List<String> payments = new LinkedList<>();
        for (int i = 0; i < receipts.size(); i++) {
            payments.add(receipts.get(i).getPayment());
        }

        return payments;
    }
  }

  public List<String> getServices(String email){

    List<Receipts> receipts;

    try(Connection t = db.open()) {

      String getQuery = "SELECT service FROM receipts WHERE email = :e";
      Query query = t.createQuery(getQuery).addParameter("e", email);
      receipts = query.executeAndFetch(Receipts.class);

    } catch (Exception ex) {
        System.err.println("Can't get sum : " + ex.getMessage());
        return null;
    }

    if (receipts == null) return null;
    else {
        List<String> services = new LinkedList<>();
        for (int i = 0; i < receipts.size(); i++) {
            services.add(receipts.get(i).getService());
        }

        return services;
    }
  }

  public List<String> getComments(String email){

    List<Receipts> receipts;

    try(Connection t = db.open()) {

      String getQuery = "SELECT comment FROM receipts WHERE email = :e";
      Query query = t.createQuery(getQuery).addParameter("e", email);
      receipts = query.executeAndFetch(Receipts.class);

    } catch (Exception ex) {
        System.err.println("Can't get sum : " + ex.getMessage());
        return null;
    }

    if (receipts == null) return null;
    else {
        List<String> comments = new LinkedList<>();
        for (int i = 0; i < receipts.size(); i++) {
            comments.add(receipts.get(i).getComment());
        }

        return comments;
    }
  }

  public boolean storeReceipt(String email, int date, double payed, String payment, String service, String comment) {
      try(Connection t = db.open()) {

          String insertQuery = "INSERT INTO receipts (email, date, payed, payment, service, comment) VALUES (:e,:d, :pd, :pt, :s, :c)";
          Query query = t.createQuery(insertQuery).addParameter("e", email).addParameter("d", date).
          addParameter("pd", payed).addParameter("pt", payment).addParameter("s", service).addParameter("c", comment);
          query.executeUpdate();

      } catch (Exception ex) {

          System.err.println("Can't store the receipt : " + ex.getMessage());
          return false;

      }
      // True if it is stored successfuly
      return true;
  }

}
