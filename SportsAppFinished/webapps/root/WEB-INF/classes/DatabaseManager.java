import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;

import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import java.text.*;

import org.mindrot.jbcrypt.BCrypt;

public class DatabaseManager {

    private Sql2o db;
    private List<Integer> occupiedIds;

    //default constructor to be used
    public DatabaseManager() {
        //establish connection with our default db
        db = new Sql2o("jdbc:sqlite:accounts.db","","");
        updateIDPool();
    }

    //used for unittesting
    public DatabaseManager(String testDBName) {
        db = new Sql2o("jdbc:sqlite:" + testDBName,"","");
        updateIDPool();
    }


    //------------Account management methods---------

    /*
     * Deletes users record from the database
     * RETURNS true if successful, false if connections with the database failed.
     */
    public boolean delete(String email) {

        if (!userExists(email)) {

            throw new IllegalArgumentException("Can't delete non-existent user");

        } else {

            try (Connection t = db.open()) {

                String insertQuery = "DELETE FROM accounts WHERE email = :u";
                Query query = t.createQuery(insertQuery).addParameter("u", email);
                query.executeUpdate();

            } catch (Exception ex) {

                System.err.println("Can't delete user : " + ex.getMessage());
                return false;

            }

        }

        return true;
    }

    /*
     * Adds user record to the database
     * RETURNS true if successful, false if connections with the database failed.
     */
    public boolean register(String email, String firstname, String lastname, String password, String role) {

        if (userExists(email)) return false;

        try (Connection t = db.open()) {

            String insertQuery = "INSERT INTO accounts (email, firstname, lastname, password, role) VALUES (:email,:fname, :lname, :password, :role)";
            String passwordHash = BCrypt.hashpw(password, BCrypt.gensalt());
            Query query = t.createQuery(insertQuery).addParameter("email", email).addParameter("fname", firstname).
            addParameter("lname", lastname).addParameter("password", passwordHash).addParameter("role", role);
            query.executeUpdate();

        } catch (Exception ex) {

            System.err.println("Can't register user : " + ex.getMessage());
            return false;

        }

        return true;

    }

    /*
     * RETURNS a list of all emails in the database
     */
    public List<String> getAllUsers () {

        List<Account> users;

        try (Connection t = db.open()) {

            String getQuery = "SELECT * FROM accounts";
            Query query = t.createQuery(getQuery);
            users = query.executeAndFetch(Account.class);

        }

        if (users == null) return null;
        else {
            List<String> emails = new LinkedList<>();
            for (int i = 0; i < users.size();i++) {
                emails.add(users.get(i).getEmail());
            }

            return emails;
        }
    }

    /*
     * RETURNS true if user exists in the database
     */
    public boolean userExists(String email) {

        if(getAllUsers().contains(email)) return true;
        else return false;

    }

    /*
     * RETURNS true if there exists user with the provided email AND this email is
     * associated with password.
     */
     public boolean match(String email, String password) {

             List<Account> account;
             try (Connection t = db.open()) {

                 String getPassQuery = "SELECT password FROM accounts WHERE email = :c";
                 Query passQuery = t.createQuery(getPassQuery).addParameter("c",email);

                 List <Account> storedPasswordHashList = passQuery.executeAndFetch(Account.class);

                 if(storedPasswordHashList.size()<1) return false;

                 Account storedPasswordHashAccount = storedPasswordHashList.get(0);
                 String storedPasswordHash = storedPasswordHashAccount.getPassword();

                 if(BCrypt.checkpw(password, storedPasswordHash))
                 {
                   String getQuery = "SELECT * FROM accounts WHERE email = :u AND password = :p";
                   Query query = t.createQuery(getQuery).addParameter("u",email).addParameter("p",storedPasswordHash);
                   account = query.executeAndFetch(Account.class);
                   return true;
                 }
                 else return false;
             }
         }


     public boolean changeRole(String email, String newrole) {

           try(Connection t = db.open()){

               String getQuery = "UPDATE accounts SET role = :role WHERE email = :email";
               Query query = t.createQuery(getQuery).addParameter("role", newrole).addParameter("email", email);
               query.executeUpdate();

               return true;
           }
           catch (Exception ex) {

               System.err.println("No account found for given email " + ex.getMessage());
               return false;

           }
     }

     public String getRole(String email){

       try(Connection t = db.open()){

           String getQuery = "SELECT role FROM accounts WHERE email = :c";
           Query query = t.createQuery(getQuery).addParameter("c", email);
           List <Account> accounts = query.executeAndFetch(Account.class);
           Account acc = accounts.get(0);
           return acc.getRole();
       }
       catch (Exception ex) {
           return null;

       }
     }

     // ------------- bankAccounts Table Methods -------------

     // Add and Delete
     public boolean addCard(String email, String cardNumber, String cardName, int expiryDate, int cvv){

       try (Connection t = db.open()) {

         String getQuery = "INSERT INTO bankAccount (cardNumber, cardName, expiryDate, cvv, email) VALUES(:cardNumber, :cardName, :expiryDate,:cvv, :email)";
         //Line below does not wrk
         // String hashedCardNumber = BCrypt.hashpw(cardNumber, BCrypt.gensalt());
         Query query = t.createQuery(getQuery).addParameter("cardNumber", cardNumber).addParameter("cardName", cardName)
         .addParameter("expiryDate", expiryDate).addParameter("cvv", cvv).addParameter("email", email);

         query.executeUpdate();
         return true;

       }
       catch(Exception ex){
         System.err.println("" + ex.getMessage());
         return false;
       }
     }

    public boolean deleteCard(String email, String cardNumber) {

         if (!userExists(email)) {
           System.out.println(email);
             throw new IllegalArgumentException("Can't delete non-existent user");

         } else {

             try (Connection t = db.open()) {

                 String insertQuery = "DELETE FROM bankAccount WHERE cardNumber = :cardNumber";
                 Query query = t.createQuery(insertQuery).addParameter("cardNumber", cardNumber);
                 query.executeUpdate();

             } catch (Exception ex) {

                 System.err.println("Can't delete bank acocunt : " + ex.getMessage());
                 return false;
             }

         }

         return true;
     }

     //---------------Timetable management methods---------

     /*
     *  ADDS facilityName to the table of facilities, which allows it to be used when registering services and
     *  capacity checks.
     *
     *  THROWS IllegalArgumentException if facilityName already exists.
     */
     void registerFacility(String facilityName, int capacity) {

       if (getFacility(facilityName).size()>0) {

           throw new IllegalArgumentException(facilityName + " already registered.");

       } else {

           try (Connection t = db.open()) {

               String regQuery = "INSERT INTO facilities (name,capacity) VALUES (:n,:c)";
               Query query = t.createQuery(regQuery).addParameter("n", facilityName)
                                        .addParameter("c",capacity);
               query.executeUpdate();

           }
       }

     }

     /*
     *  REMOVES entry identified by facilityName
     *
     *  THROWS IllegalArgumentException if facilityName does not exist.
     */
    void unregisterFacility(String facilityName) {

      if (getFacility(facilityName).size()<1) {

          throw new IllegalArgumentException(facilityName + " not registered -- can't be unregistered.");

      } else {

          try (Connection t = db.open()) {

              String regQuery = "DELETE FROM facilities WHERE name = :n";
              Query query = t.createQuery(regQuery).addParameter("n", facilityName);
              query.executeUpdate();

          }
      }
    }

    /*
    *     RETURNS a list of facilities identified by facilityName
    *     or NULL if no facilities are registered in the db.
    *     IF all goes well the list should contain 1 or less items. Otherwise, some db constraints have been violated.
    */
    public List<Facility> getFacility(String facilityName) {

      List<Facility> f;

      try (Connection t = db.open()) {

        String getQuery = "SELECT * FROM facilities WHERE name = :n";
        Query query = t.createQuery(getQuery).addParameter("n",facilityName);
        f = query.executeAndFetch(Facility.class);

      }

      return f;

    }

    /*
    * Returns a list of Facility objects representing each registered facility.
    */
    public List<Facility> getAllFacilities() {

      List<Facility> f;

      try (Connection t = db.open()) {

        String getQuery = "SELECT * FROM facilities";
        Query query = t.createQuery(getQuery);
        f = query.executeAndFetch(Facility.class);

      }

      if (f == null) return null;
      else return f;

    }

    /*
    * RETURNS true if Facility identified by fName is registered in the database, false otherwise.
    */
    public boolean facilityExists(String fName) {

      List<Facility> f = getAllFacilities();

      for (Facility fc : f) {
        if(fc.getName().equals(fName)) return true;
      }

      return false;
    }

    /*
    * Called from withing the constructor to initialise service ID system.
    */
    private void updateIDPool() {

      occupiedIds = new LinkedList<>();
      List<Service> allS = getAllServices();

      for (Service s : allS) {

        occupiedIds.add(s.getID());

      }

    }

    /*
    * Returns an unused ID to use for some Service
    */
    private int getNewID() {

      for (int i = 0; i<99999; i++) {
        if(!occupiedIds.contains(Integer.valueOf(i))) {
          occupiedIds.add(Integer.valueOf(i));
          return i;
        }
      }
      throw new RuntimeException("ID pool exhaused, can't find free id.");

    }


     /*
      * Adds new service to the services table;
      * NOTE that time is kept as Strings to avoid complicated formatting :
      * e.g. 1300 = "1300", 5:00 = "0500" etc
      */

     void addServiceTimeSlot(String serviceName, String date, String startTime, String endTime, String facility, int capacity, int price) {

       //step 1 : check entry is not a duplicate
       if(serviceExists(serviceName,date,startTime,endTime,facility)) {
         throw new IllegalArgumentException("Attempting to add duplicate service to the database.");
       }

       // step 2 : check facility exists
       List<Facility> allF = getAllFacilities();
       List<String> facilityNames = new LinkedList<>();
       for (int i = 0; i<allF.size();i++) {
         facilityNames.add(allF.get(i).getName());
       }
       if(!facilityNames.contains(facility)) {
         throw new IllegalArgumentException("Facility '" + facility + "' does not exist.");
       }

       //step 3 : register service
       try (Connection t = db.open()) {

         String addQuery = "INSERT INTO services (id,serviceName,date,startTime,endTime,facility,capacity,price) VALUES (:i,:n,:d,:s,:e,:f,:c,:p)";
         Query query = t.createQuery(addQuery).addParameter("i",getNewID()).addParameter("n",serviceName).addParameter("d",date).addParameter("s",startTime)
                                .addParameter("e",endTime).addParameter("f",facility).addParameter("c",capacity).addParameter("p",price);
         query.executeUpdate();

       }
     }

     /*
      * Removes an exact time slot of a service given its attributes;
      * NOTE for time format see addService(..)
      */
     void removeServiceTimeSlot(String serviceName, String date, String startTime, String endTime, String facility) {

       if(!serviceExists(serviceName,date,startTime,endTime,facility)) {
         throw new IllegalArgumentException("Attempting to delete service that does not exist.");
       }

       try (Connection t = db.open()) {

         String removeQuery = "DELETE FROM services WHERE serviceName = :n AND date = :d AND startTime = :s AND endTime = :e AND facility = :f";
         Query query = t.createQuery(removeQuery).addParameter("n",serviceName).addParameter("d",date).addParameter("s",startTime)
                                .addParameter("e",endTime).addParameter("f",facility);
         query.executeUpdate();

       }

       updateIDPool();
     }


     /*
      * Removes an exact time slot of a service given a Service object representing that service;
      * NOTE this version exists to be used easily with getAllServices()
      * NOTE for time format see addService(..)
      */
     void removeServiceTimeSlot(Service removable) {

       if(!serviceExists(removable)) {
         throw new IllegalArgumentException("Attempting to delete service that does not exist.");
       }

       try (Connection t = db.open()) {

         String removeQuery = "DELETE FROM services WHERE serviceName = :n AND date = :d AND startTime = :s AND endTime = :e AND facility = :f";
         Query query = t.createQuery(removeQuery).addParameter("n",removable.getName()).addParameter("d",removable.getDate())
                                .addParameter("s",removable.getStartTime()).addParameter("e",removable.getEndTime()).addParameter("f",removable.getFacility());
         query.executeUpdate();

       }

       updateIDPool();
     }

     /*
      * Returns a list of all service slots or NULL if no services are in the database.
      * This method is intended to be used for the main timetable.
      */
     List<Service> getAllServices() {

         List<Service> s;

         try (Connection t = db.open()) {

           String getAllQuery = "SELECT * FROM services";
           Query query = t.createQuery(getAllQuery);
           s = query.executeAndFetch(Service.class);

         }

         return s;
      }

    /*
    * Returns TRUE if exact same service is already in the database
    * NOTE that capacity and price aren't taken into account as that would be redundant.
    */
    boolean serviceExists(String serviceName, String date, String startTime, String endTime, String facility) {
      List<Service> s;

      try (Connection t = db.open()) {

          String getAllQuery = "SELECT * FROM services WHERE serviceName = :n AND date = :d AND startTime = :s AND endTime = :e AND facility = :f";
          Query query = t.createQuery(getAllQuery).addParameter("n",serviceName).addParameter("d",date).addParameter("s",startTime)
                                .addParameter("e",endTime).addParameter("f",facility);
          s = query.executeAndFetch(Service.class);
      }

      if (s.size()>0) return true;
      else return false;
    }

    public void reduceCapacity(int id, int newCapacity) {

      List<Service> listId;
      try (Connection t = db.open()) {

          String getAllQuery = "SELECT * FROM services WHERE id = :id";
          Query queryCheck = t.createQuery(getAllQuery).addParameter("id",id);
          listId = queryCheck.executeAndFetch(Service.class);
      }
      if (listId.size()!=1) {
        System.out.println("Should exist exacly one query with given id.");
        return;
      }

      try (Connection t = db.open()) {
        String rmQuery = "UPDATE services SET capacity = :capacity WHERE id = :id";
        Query query = t.createQuery(rmQuery).addParameter("capacity", newCapacity).addParameter("id", id);
        query.executeUpdate();
      }
      catch (Exception ex) {
          System.err.println("Unknown Exception");

      }

    }

    /*
    * Returns TRUE if service in the database;
    */
    public boolean serviceExists(Service service) {
      List<Service> s = getAllServices();

      if (s.contains(service)) return true;
      else return false;
    }

    /*
     *  ADDS a record that user "email" is booked service "serviceName"
     *  THROWS IllegalArgumentException if server or user do not exist
     *  THROWS RuntimeException if subscription is already in the records.
     */
    boolean bookService(String email, Service service) {

      if (!userExists(email)) {
        //throw new IllegalArgumentException("No such user : " + email);
        return false;
      }

      if (!serviceExists(service)) {
        //throw new IllegalArgumentException("No such service : " + service);
        return false;
      }


      if (userHasBooking(email,service)) {
        //throw new RuntimeException(service + " is already booked by " + email);
        return false;
      }

      try (Connection t = db.open()) {
        String addQuery = "INSERT INTO bookings (email,serviceid) VALUES (:e, :i)";
        Query query = t.createQuery(addQuery).addParameter("e",email).addParameter("i",service.getID());
        query.executeUpdate();
        return true;
      }
    }

    /*
    * RETURNS true if user <email> has previously booked service<service>
    * returns false otherwise
    */
    public boolean userHasBooking(String email, Service service) {

      List<Booking> allForUser = getUserBookings(email);
      for (Booking b : allForUser) if (b.getServiceID() == service.getID()) return true;
      return false;

    }

    /*
     *  REMOVES record that user "email" is subscribed to service "serviceName"
     *  THROWS IllegalArgumentException if server or user do not exist
     *  THROWS RuntimeException if no such subscription is in the records.
     */
    void cancelBookingOfService(String email, Service service) {

      if (!userExists(email)) {
        throw new IllegalArgumentException("No such user : " + email);
      }

      if (!serviceExists(service)) {
        throw new IllegalArgumentException("No such service : " + service);
      }

      if (!userHasBooking(email,service)) {
        throw new RuntimeException(email + " has not booked " + service);
      }

      try (Connection t = db.open()) {

        String addQuery = "DELETE FROM bookings WHERE email = :e AND serviceid = :i";
        Query query = t.createQuery(addQuery).addParameter("e",email).addParameter("i",service.getID());
        query.executeUpdate();

      }

    }


    /*
    * RETURNS a list of all subsciptions a user has.
    */
    List<Booking> getUserBookings(String email) {

      if (!userExists(email)) {
        throw new IllegalArgumentException("No such user : " + email);
      }

      List<Booking> b;
      try (Connection t = db.open()) {

          String getAllQuery = "SELECT * FROM bookings WHERE email = :e";
          Query query = t.createQuery(getAllQuery).addParameter("e",email);
          b = query.executeAndFetch(Booking.class);
      }
      return b;
    }


    /*
    * RETURNS a list of Subscription objects for every subscription
    */
    List<Booking> getAllBookings() {

      List<Booking> b;
      try (Connection t = db.open()) {

          String getAllQuery = "SELECT * FROM bookings";
          Query query = t.createQuery(getAllQuery);
          b = query.executeAndFetch(Booking.class);
      }

      return b;
    }

    public List<Membership> getMembershipData(String email) {

      List<Membership> m;
      try (Connection t = db.open()) {

          String getQuery = "SELECT * FROM members WHERE email = :e";
          Query query = t.createQuery(getQuery).addParameter("e",email);
          m = query.executeAndFetch(Membership.class);
        }

        return m;
    }


    public void registerMembership(String email, String type, String expiryDate) {

      if (!userExists(email)) {
        throw new IllegalArgumentException("No such user : " + email);
      }
      if (!Membership.isValidDateFormat(expiryDate)) {
        throw new IllegalArgumentException("Invalid date formatting, expected dd.mm.yyyy received " + expiryDate);
      }

      try (Connection t = db.open()) {

        String addQuery = "INSERT INTO members (email, type, expiryDate) VALUES (:e,:t,:d)";
        Query query = t.createQuery(addQuery).addParameter("e",email).addParameter("t",type).addParameter("d",expiryDate);
        query.executeUpdate();
      }

    }

    public void removeMembership(String email) {

      if (!userExists(email)) {
        throw new IllegalArgumentException("No such user : " + email);
      }
      if (getMembershipData(email).size()<1) {
        throw new IllegalArgumentException(email + " has no memberships, can't remove any.");
      }
      if(getMembershipData(email).size()>1) {
        System.out.println("WARNING : " + email + " has several memberships registered.");
      }

      try (Connection t = db.open()) {

        String rmQuery = "DELETE FROM members WHERE email = :e";
        Query query = t.createQuery(rmQuery).addParameter("e",email);
        query.executeUpdate();

      }

    }

 }
