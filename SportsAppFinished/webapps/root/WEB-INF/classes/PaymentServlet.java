import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class PaymentServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("pages/payment.jsp").forward(request,response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		PrintWriter out = response.getWriter();
		//Passing arguments from Registration
		String email = (String)request.getSession().getAttribute("name");
		String first_name = (String)request.getSession().getAttribute("firstname");
		String last_name = (String)request.getSession().getAttribute("lastname");
		String password = (String)request.getSession().getAttribute("password");
		request.getSession().invalidate();

		String card_number = request.getParameter("cardnumber");
		String card_name = request.getParameter("cardname");
		int cvv = Integer.parseInt(request.getParameter("cvv"));
    int exp_date = Integer.parseInt(request.getParameter("exp"));
		DatabaseManager database = new DatabaseManager();


		try {

						if(checkCard(card_number)){
							System.out.println("Valid");
							//If the card is valid, register the user and add card.
							database.register(email, first_name, last_name, password, "member");


							//Temp, since there is no Exp_date captured (only 4digits , when 6is required for DB)
							exp_date=123456;
							database.registerMembership(email, "premium", "12.34.56"); //By default give premium membership
							database.addCard(email, card_number, card_name, exp_date, cvv);
							request.getSession().setAttribute("name", email);
							}
						else{
								System.out.println("Invalid");
						}

        }
        catch(Exception se) {
					System.out.println("this one deos not work..");
          se.printStackTrace();
        }
				response.sendRedirect("/");
	}

	//Function works WITHOUT SPACES!
public static boolean checkCard(String card_number)
  {
        int sum = 0;
        boolean alternate = false;
        for (int i = card_number.length() - 1; i >= 0; i--)
        {
                int n = Integer.parseInt(card_number.substring(i, i + 1));
                if (alternate)
                {
                        n *= 2;
                        if (n > 9)
                        {
                                n = (n % 10) + 1;
                        }
                }
                sum += n;
                alternate = !alternate;
        }
        return (sum % 10 == 0);
      }

}
