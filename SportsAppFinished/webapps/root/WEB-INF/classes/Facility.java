public class Facility {

  private String name;
  private int capacity;

  public String toString() {
    return name + "(" + String.valueOf(capacity) + ")";
  }

  public String getName() {
    return name;
  }

  public int getCapacity() {
    return capacity;
  }
}
