public class Booking {

  String email;
  int serviceid;

  public String getEmail() {
    return email;
  }

  public int getServiceID() {
    return serviceid;
  }

  public int getServiceid() {
    return serviceid;
  }

  public String toString() {
    return email + " booked service " + serviceid;
  }

}
