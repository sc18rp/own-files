import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class RegistrationServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("pages/register.jsp").forward(request,response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		PrintWriter out = response.getWriter();
		String first_name = request.getParameter("firstname");
    String last_name = request.getParameter("lastname");
    String email = request.getParameter("email");
	String password = request.getParameter("password");
	String checkEmail = "";
	String checkPw = "";
		String password_check = request.getParameter("confirmpassword");
		DatabaseManager database = new DatabaseManager();

		if(database.userExists(email)){
			checkEmail = "detected";
			request.getSession().setAttribute("checkEmail",checkEmail);
		}
		if(password.equals(password_check)){
			checkPw = "detected";
			request.getSession().setAttribute("checkPw",checkPw);
		}
		try {
						if(!password.equals(password_check) || database.userExists(email) ){
								System.out.println("BAD");
							}
						else{
								System.out.println("GOOD");
								request.getSession().setAttribute("name", email);
								request.getSession().setAttribute("firstname", first_name);
								request.getSession().setAttribute("lastname", last_name);
								request.getSession().setAttribute("password", password);
						}
        }
        catch(Exception se) {
					System.out.println("this one does not work..");
          se.printStackTrace();
        }
				response.sendRedirect("/payment");
	}
}
