import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class EmployServlet extends HttpServlet {


  @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    DatabaseManager db = new DatabaseManager();
    String username = request.getSession().getAttribute("user").toString();

    if(db.getRole(username) != null && "admin".equals(db.getRole(username))){
      request.getRequestDispatcher("adminpages/employ.jsp").forward(request,response);
    }
    else{
      response.sendRedirect("/");
    }
	}

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    String email = request.getParameter("email");
    DatabaseManager db = new DatabaseManager();


    if(db.changeRole(email, "employee")){
      response.sendRedirect("/employ?err=false");
    }
    else{
      response.sendRedirect("/employ?err=true");
    }
  }
}
