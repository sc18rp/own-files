import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class DisplayTimetableSquashServlet extends HttpServlet {

  private String type = null;
  private String length = null;
  private String payment = null;

  @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
      DatabaseManager database = new DatabaseManager();
      List<Service> services = database.getAllServices();
      List <Service> squashServices = new ArrayList<>();
      List<String> facilities = new ArrayList<String>();
      List<String> servicenames = new ArrayList<String>();



      for(int i = 0; i < services.size(); i++) {
        Service service = services.get(i);
        if(service.getFacility().equals("Squash Courts") && service.getCapacity() > 0) {
          squashServices.add(service);
        }
        facilities.add(service.getFacility());
        servicenames.add(service.getName());
        System.out.println(services.toString());
      }
      request.getSession().setAttribute("facilities", facilities);
      request.getSession().setAttribute("servicenames", servicenames);
      request.getSession().setAttribute("services", services);
      request.getSession().setAttribute("squashServices", squashServices);

      if(request.getSession().getAttribute("membership") != null){
        request.getRequestDispatcher("pages/displaySquash.jsp").forward(request,response);
      }
      else{
        response.sendRedirect("/");
      }
	}

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    DatabaseManager database = new DatabaseManager();
  }
}
