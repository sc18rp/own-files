import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class BookingServlet extends HttpServlet {

  private String type = null;
  private String length = null;
  private String payment = null;

  @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
      DatabaseManager database = new DatabaseManager();
      String email = (String)request.getSession().getAttribute("user");
      List<Booking> userBookings = database.getUserBookings(email);
      List <Service> bookingService = new ArrayList<>();
      List<Service> services = database.getAllServices();
      for(int i = 0; i < userBookings.size(); i++)
      {
        Booking booking = userBookings.get(i);
        int bookingid = booking.getServiceID();
        Service service = services.get(bookingid);
        bookingService.add(service);
      }
      request.getSession().setAttribute("bookingService", bookingService);
      if(request.getSession().getAttribute("membership") != null){
        request.getRequestDispatcher("pages/userBookings.jsp").forward(request,response);
      }
      else{
        response.sendRedirect("/");
      }
	}

}
