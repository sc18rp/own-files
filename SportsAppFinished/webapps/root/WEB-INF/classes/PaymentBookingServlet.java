import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class PaymentBookingServlet extends HttpServlet {

  private String type = null;
  private String length = null;
  private String payment = null;

  @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
      DatabaseManager database = new DatabaseManager();
      List<Service> services = database.getAllServices();
      List <Service> bookingService = new ArrayList<>();
      String id = (String)request.getParameter("id");
      int idint = Integer.parseInt(id);
      Service service = services.get(idint);
      bookingService.add(service);
      System.out.println(service.toString());
      request.getSession().setAttribute("bookingService", bookingService);
      request.getSession().setAttribute("service", service);
      request.getRequestDispatcher("pages/paymentBooking.jsp").forward(request,response);
	}

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    DatabaseManager database = new DatabaseManager();
    PrintWriter out = response.getWriter();
    Service service  = (Service)request.getSession().getAttribute("service");
    String email = (String)request.getSession().getAttribute("user");
    String card_number = request.getParameter("cardnumber");
    String card_name = request.getParameter("cardname");
    int cvv = Integer.parseInt(request.getParameter("cvv"));
    int exp_date = Integer.parseInt(request.getParameter("exp"));

    try {

            if(checkCard(card_number)){
              if(database.bookService(email,service)) //Book service. Error is displayed if the current user already booked this service.
              {
                System.out.println("BOOKED BY" + email);
                int newCapacity = service.getCapacity()-1;
                database.reduceCapacity(service.getID(), newCapacity);
              }
              else
              {
                response.sendRedirect("/bookingError");
              }
              }
            else{
                System.out.println("Invalid");
            }

        }
        catch(Exception se) {
          System.out.println("this one does not work..");
          se.printStackTrace();
        }
        response.sendRedirect("/paymentConfirmation");
  }

  //Function works WITHOUT SPACES!
  public static boolean checkCard(String card_number)
  {
        int sum = 0;
        boolean alternate = false;
        for (int i = card_number.length() - 1; i >= 0; i--)
        {
                int n = Integer.parseInt(card_number.substring(i, i + 1));
                if (alternate)
                {
                        n *= 2;
                        if (n > 9)
                        {
                                n = (n % 10) + 1;
                        }
                }
                sum += n;
                alternate = !alternate;
        }
        return (sum % 10 == 0);
      }
}
