public class Receipts {

    private String email;
    private int date;
    private double payed;
    private String payment;
    private String service;
    private String comment;

    public String getEmail() {
        return email;
    }

    public int getDate(){
      return date;
    }

    public double getPayed(){
      return payed;
    }

    public String getPayment(){
      return payment;
    }

    public String getService(){
      return service;
    }

    public String getComment(){
      return comment;
    }
}
