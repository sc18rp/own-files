public class BankAccount {

    private String email;
    private int cardNumber;
    private String cardName;
    // Expiry date will be converted from foe example 2020/02 to 202002
    private int expiryDate;
    private int cvv;

    public String getEmail() {
        return email;
    }

    //Used in testing
    public String getCardName() {
        return cardName;
    }
    public int getCardNumber() {
        return cardNumber;
    }

}

