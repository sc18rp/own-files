import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

public class Service {

  private int id;
  private String serviceName;
  private String date;
  private String startTime;
  private String endTime;
  private String facility;
  private int capacity;
  private int price;

  public int getID() {
    return id;
  }

  public int getId() {
    return id;
  }

  public int reduceCapacityByOne() {
    capacity = capacity - 1;
    return capacity;
  }

  public String getName() {
    return serviceName;
  }

  public String getServiceName() {
    return serviceName;
  }

  public int getPrice() {
    return price;
  }

  public String getStartTime() {
    return startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public String getDate() {
    return date;
  }

  public Date getDateAsDateObj() throws ParseException {
    Date dateNow = new Date();
    Date serviceDate = new SimpleDateFormat("dd.mm.yyyy").parse(date);
    return serviceDate;
  }

  public String getFacility() {
    return facility;
  }

  public int getCapacity() {
    return capacity;
  }

  public String getStartTimeAsFormattedString() {
    return startTime.substring(0,2) + ":" + startTime.substring(2,4);
  }

  public String getEndTimeAsFormattedString() {
    return endTime.substring(0,2) + ":" + endTime.substring(2,4);
  }

  public String toString() {
    return String.format("[%d] %s (%s, %s-%s) at %s",id, serviceName, date, getStartTimeAsFormattedString(), getEndTimeAsFormattedString(), facility);
  }

  //NOTE that without .equals, .contains() will not behave as expected
  public boolean equals(Object o) {

    if (this == o) {

      return true;

    } else if(o instanceof Service) {

      Service comparable = (Service) o;
      if(comparable.id == id) return true;

    }

    return false;
  }
}
