from __future__ import print_function
from copy import deepcopy

# Representation of a state:
# (move_number, full_board_state)
# Example ( 3, [[1,1,1],[1,1,0],[1,0,1]] )

# Make all board elements as 0's for the intial queen move
def qc_initial_state(x,y):
    return ( 0, matrix_of_zeros(y,x) )

# Marking 0's
def matrix_of_zeros(X,Y):
    return [ [0 for x in range(X)] for y in range(Y)]

# Function adding moves to a list of
# where the Qeen can go (marked zero and one since queens are not threatening each other)
def qc_possible_actions( state ):
    # Making the moves list empty
    moves = []
    for x in range(BOARD_X):
        for y in range(BOARD_Y):
            if(state[1][x][y]==0 or state[1][x][y]==1):
                # Adding moves to a list if it is marked as 0 or 1
                moves = moves + [(x,y)]
    return moves

# Changing the 0's to 1's by the queens actions
def qc_successor_state( action, state ):
    # Getting the board
    board = deepcopy(state[1])

    # Getting Queens action position coordinates
    xpos = action[0]
    ypos = action[1]

    # Making <->  (Horizontal) as 1
    for tempX in range(BOARD_X):
        board[tempX][ypos] = 1

    # Making (Vertical) as 1
    for tempY in range(BOARD_Y):
        board[xpos][tempY] = 1

    # Making diagonal top left as 1
    tempY = ypos
    tempX = xpos
    while tempX>=0 and tempY>=0 and tempY<BOARD_Y and tempX<BOARD_X:
        board[tempX][tempY] = 1
        tempY-=1
        tempX-=1

    # Making diagonal top right as 1
    tempY = ypos
    tempX = xpos
    while tempX>=0 and tempY>=0 and tempY<BOARD_Y and tempX<BOARD_X:
        board[tempX][tempY] = 1
        tempY-=1
        tempX+=1

    # Making diagonal bottom right as 1
    tempY = ypos
    tempX = xpos
    while tempX>=0 and tempY>=0 and tempY<BOARD_Y and tempX<BOARD_X:
        board[tempX][tempY] = 1
        tempY+=1
        tempX+=1

    # Making diagonal bottom left as 1
    tempY = ypos
    tempX = xpos
    while tempX>=0 and tempY>=0 and tempY<BOARD_Y and tempX<BOARD_X:
        board[tempX][tempY] = 1
        tempY+=1
        tempX-=1

    # Incrementing the move number by one as the board changed it's 0's to 1's where required
    movenum = state[0] + 1

    # The queen's position is marked as 2, so that queens could not overlap
    board[action[0]][action[1]] = 2

    # returning move number and current board state
    return (movenum, board)

# Testing if there are any possible moves for a queen in a list
def qc_test_goal_state( state ):
    # if there is no 0's on a board, it means it is a goal state
    for x in range(BOARD_X):
        for y in range(BOARD_Y):
            if(state[1][x][y]==0):
                return False
    return True

# Printing the number of rows and columns in a board
def queen_print_problem_info():
    print( "The Queen Problem (", BOARD_X, "x", BOARD_Y, "board)" )

## Return a problem spec tuple for a given board size
def make_qc_problem(x, y):
    global BOARD_X, BOARD_Y, qc_initial_state
    BOARD_X = x
    BOARD_Y = y
    return  ( None,
                queen_print_problem_info,
                qc_initial_state(x,y),
                qc_possible_actions,
                qc_successor_state,
                qc_test_goal_state
              )
