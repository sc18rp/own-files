var b = 0, d = 0; //born / died
var total;
var seconds=0, minutes = 0, hours = 0, days = 0;
var po = 0, cr = 0, mu = 0, hi = 0;
var su = 0, ca = 0, st = 0, heart = 0;
var asia = 0, namerica = 0, lamerica = 0, europe = 0, oceania = 0, africa = 0;
function increment()
{
  b++;
  d=(d+0.42);
  po+=0.41;
  cr+=0.3072;
  mu+=0.2232;
  hi+=0.1536;
  su+=0.006;
  st+=0.072;
  ca+=0.0096;
  heart = heart + (0.42*0.235);

  document.getElementById('births').innerHTML = b;
  document.getElementById('deaths').innerHTML = Math.round(d);
  document.getElementById('total').innerHTML = b - Math.round(d);
  document.getElementById('poverty').innerHTML = Math.round(po);
  document.getElementById('Christian').innerHTML = Math.round(cr);
  document.getElementById('Muslim').innerHTML = Math.round(mu);
  document.getElementById('Hindu').innerHTML = Math.round(hi);
  document.getElementById('suicide').innerHTML = Math.round(su);
  document.getElementById('car').innerHTML = Math.round(ca);
  document.getElementById('starvation').innerHTML = Math.round(st);
  document.getElementById('heart').innerHTML = Math.round(heart);

  document.getElementById('asia').innerHTML = Math.round((b - Math.round(d))*0.46);
  document.getElementById('europe').innerHTML = Math.round((b - Math.round(d))*0.006216);
  document.getElementById('africa').innerHTML = Math.round((b - Math.round(d))*0.38);
  document.getElementById('namerica').innerHTML = Math.round((b - Math.round(d))*0.031);
  document.getElementById('lamerica').innerHTML = Math.round((b - Math.round(d))*0.114);
  document.getElementById('oceania').innerHTML = Math.round((b - Math.round(d))*0.0081);
}

setInterval('increment()', 240);
//total time
function time_count()
{
  if(seconds>=59)
  {
    minutes++;
    seconds = 0;
    document.getElementById('minutes').innerHTML = minutes;
    document.getElementById('seconds').innerHTML = seconds;
  }
  else
  if(minutes>=59)
  {
    hours++;
    minutes=0;
    seconds=0;
    document.getElementById('hours').innerHTML = hours;
    document.getElementById('minutes').innerHTML = minutes;
    document.getElementById('seconds').innerHTML = seconds;
  }
  else
  if(hours>=23)
  {
    days++;
    hours=0;
    minutes=0;
    seconds=0;
    document.getElementById('days').innerHTML = days;
    document.getElementById('hours').innerHTML = hours;
    document.getElementById('minutes').innerHTML = minutes;
    document.getElementById('seconds').innerHTML = seconds;
  }
  else
  {
    seconds++;
    document.getElementById('seconds').innerHTML = seconds;
  }
}
setInterval('time_count()', 1000); //1000ms = 1s
