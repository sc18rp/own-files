var position = 0, answered = 0;
var test, progress, question, choice, choices, first, second, third;
//Database of Questions and Answers
var questions=
[
  ["What is the largest country by area?", "Russia", "Canada", "United States", "A"],
  ["What is the largest country by population?", "Russia", "India", "China", "C"],
  ["What is the smallest country by area?", "Monaco", "Tuvalu", "Vatican City", "C"],
  ["What is the smallest country by population?", "Niue", "Vatican City", "Falkland Islands", "B"],
  ["What is the area of Canada?", "~5 000 000 km<sup>2</sup>", "~10 000 000km<sup>2</sup>", "~50 000 000km<sup>2</sup>", "B"]
];

function make()
{
  test = document.getElementById("test");

  //When the quiz is complete (5questions in this case)
  if(position >= questions.length)
  {
    test.innerHTML = "<h2>You Got " + answered + " of " + questions.length + " Questions Correct</h2>";
    document.getElementById("progress").innerHTML = "Test Completed";
    position = 0;
    answered = 0;
    return 0;
  }

  document.getElementById("progress").innerHTML = "Question " + (position + 1) + " of " + questions.length;

  //Generating question/answer
  question = questions[position][0];
  first = questions[position][1];
  second = questions[position][2];
  third = questions[position][3];

  //making question and answer options appear
  test.innerHTML = "<h3>" + question + "</h3>";
  test.innerHTML += "<input type='radio' name='choices' value='A'> " + first + "<br>";
  test.innerHTML += "<input type='radio' name='choices' value='B'> " + second + "<br>";
  test.innerHTML += "<input type='radio' name='choices' value='C'> " + third + "<br><br>";
  test.innerHTML += "<button class='submit' onclick='check()'>Submit Answer</button>";
}

//Check if the answer is correct
function check()
{
  choices = document.getElementsByName("choices");
  var x = 0;
  for(x; x<choices.length; x++)
  {
    if(choices[x].checked)
    {
      choice = choices[x].value;
    }
  }

  if(choice == questions[position][4])
  {
    answered++;
  }
  position++;
  //Go to make again
  make();
}
window.addEventListener("load", make, false);
