/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content*/
var dp = document.getElementsByClassName("dropdown-btn");

var x;
for (x = 0; x < dp.length; x++)
{
  dp[x].addEventListener("click", function()
    {
      this.classList.toggle("active");
      var drop = this.nextElementSibling;
      if (drop.style.display === "block")
      {
        drop.style.display = "none";
      }
      else
      {
        drop.style.display = "block";
      }
    });
}
