var map; //For loading the map
var box; //For new InfoWindow object

function initMap()
{
  map = new google.maps.Map(document.getElementById('map'),
  {
    //Leeds by default
    center: {lat: 53.801277, lng: -1.548567},
    zoom: 16
  });
  box = new google.maps.InfoWindow;

  if (navigator.geolocation)
  {
    navigator.geolocation.getCurrentPosition(function(position)
    {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      box.setPosition(pos);
      box.setContent('You are right here');
      box.open(map);
      map.setCenter(pos);
    },
    function()
    {
      handleLocationError(true, box, map.getCenter());
    });
    }
    else
    {
      //In case browser does not support
      handleLocationError(false, box, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, box, pos)
{
  box.setPosition(pos);
  box.setContent(browserHasGeolocation ?
    'Error: The Geolocation service failed.' :
    'Error: Your browser doesn\'t support geolocation.');
  box.open(map);
}
